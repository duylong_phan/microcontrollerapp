﻿using LongModel.ViewModels;
using MicroControllerApp.Models;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.ViewModels.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MicroControllerApp.Models.DataClasses.Packages;
using OxyPlot;
using MicroControllerApp.Models.ViewClasses.Graphics;
using System.Windows;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using MicroControllerApp.Interfaces;

namespace MicroControllerApp.ViewModels
{
    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        #region Local
        private void RfTrans_InitializeSW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as RfTransSetting;
            Setting_InitializeSW(setting);            
        }

        private void RfTrans_InitializeHW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as RfTransSetting;
            Setting_InitializeHW(setting);
        }

        private void RfTrans_Update()
        {
            if (this.packageQueue.Count == 0)
            {
                return;
            }

            var setting = this.InputParameter.CurrentConfig.CurrentSetting as RfTransSetting;
            var sampleCollection = new List<double[]>();
            DataPackage package = null;

            #region Get Sample
            while (this.packageQueue.Count > 0)
            {
                this.packageQueue.TryDequeue(out package);
                if (package is ISharedPackage)
                {
                    Setting_Update(setting, package as ISharedPackage);
                }
                else if (package is RfTransCountPackage)
                {
                    var countPackage = package as RfTransCountPackage;
                    var sample = new double[2] { 0, countPackage.Count };
                    var amount = DateTime.Now.Ticks - this.startTick;
                    sample[0] = amount / (double)TimeSpan.TicksPerSecond;
                    sampleCollection.Add(sample);
                }
                else if (package is RfTransStatusPackage)
                {
                    var statusPackage = package as RfTransStatusPackage;
                    if (statusPackage.Status == RfTransStatus.OperDone)
                    {
                        if (setting.SyncMatFile)
                        {
                            this.StopMatHandler.TryToExecute();
                        }
                        this.OutputParameter.UpdateAppStatus("Device operation was completed");
                    }
                    this.hasResponse = true;
                    this.lastResponse = package;                    
                }
            }
            #endregion

            #region View
            var pointCollection = new List<DataPoint>();
            var curve = this.OutputParameter.PlotModel.Series[0] as DataCurve;

            //Get Graphic Point
            foreach (var item in sampleCollection)
            {
                pointCollection.Add(new DataPoint(item[0], item[1]));
            }

            //Check Overflow
            var remain = setting.GraphicSetting.PointAmount - curve.Points.Count;
            var overflow = pointCollection.Count - remain;
            if (overflow > 0)
            {
                curve.Points.RemoveRange(0, overflow);
            }
            //Add Point
            curve.Points.AddRange(pointCollection);

            //View is available
            if (this.OutputParameter.HasView(AppViews.Graphic))
            {
                this.OutputParameter.UpdateGraphic();
            }
            #endregion

            #region Mat File
            if (this.matFileHandler != null && this.matFileHandler.IsEnabled)
            {
                foreach (var item in sampleCollection)
                {
                    this.matFileHandler.Add(item);
                }
                this.OutputParameter.MatSampleAmount = this.matFileHandler.SampleAmount;
            }
            #endregion
        }

        private void RfTrans_DisposeSW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as RfTransSetting;
            Setting_DisposeSW(setting);
        }

        private void RfTrans_DisposeHW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as RfTransSetting;
            Setting_DisposeHW(setting);
        }
        #endregion

        #region Remote
        private void RfTrans_OnRequested(HttpListenerRequest request, HttpListenerResponse respone)
        {
            throw new NotImplementedException();
        }

        private void RfTrans_OnResponsed(string arg1, byte[] arg2)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Command
        private bool Can_RfTrans_GetStatus()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_RfTrans_GetStatus()
        {
            try
            {
                var payload = RfTransAction.Ping;
                var rfPackage = new RfPackage((byte)MicroTasks.RfTrans, (byte)RfTransTypes.Status, payload.GetBytes());

                this.hasResponse = false;
                WriteOut(rfPackage);                
                await Task.Delay(RfTrans_WaitResponse);

                if (this.hasResponse && this.lastResponse is RfTransStatusPackage)
                {
                    this.OutputParameter.UpdateAppStatus("Device is available");
                }
                else
                {
                    throw new Exception("Device is not available");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Get Device Status error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_RfTrans_WriteSetting()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_RfTrans_WriteSetting()
        {
            try
            {
                var setting = this.InputParameter.CurrentConfig.CurrentSetting as RfTransSetting;
                var payload = setting.OperSetting;
                var rfPackage = new RfPackage((byte)MicroTasks.RfTrans, (byte)RfTransTypes.Status, payload.GetBytes());
                
                this.hasResponse = false;
                WriteOut(rfPackage);
                await Task.Delay(RfTrans_WaitResponse);

                if (this.hasResponse && lastResponse is RfTransStatusPackage)
                {
                    var response = lastResponse as RfTransStatusPackage;
                    if (response.Status == RfTransStatus.WriteSettingDone)
                    {                        
                        this.OutputParameter.UpdateAppStatus("Setting was written successfully to device");
                    }
                    else
                    {
                        throw new Exception("Invalid Response from Device. Please try gain, or check the firmware");
                    }
                }
                else
                {
                    throw new Exception("No Acknowledge response from device. Please try again");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Write Setting to Device error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_RfTrans_StartOper()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_RfTrans_StartOper()
        {
            try
            {
                var setting = this.InputParameter.CurrentConfig.CurrentSetting as RfTransSetting;
                var payload = RfTransAction.StartOper;
                var rfPackage = new RfPackage((byte)MicroTasks.RfTrans, (byte)RfTransTypes.Status, payload.GetBytes());

                this.hasResponse = false;
                WriteOut(rfPackage);
                await Task.Delay(RfTrans_WaitResponse);

                if (this.hasResponse && this.lastResponse is RfTransStatusPackage)
                {
                    var response = lastResponse as RfTransStatusPackage;
                    if (response.Status == RfTransStatus.OperWorking)
                    {
                        if (setting.SyncMatFile)
                        {
                            this.StartMatHandler.TryToExecute();
                        }

                        //set, reset
                        this.startTick = DateTime.Now.Ticks;
                        this.OutputParameter.ClearGraphic();
                        this.OutputParameter.UpdateAppStatus("Device operation was started");
                    }
                    else
                    {
                        throw new Exception("Invalid Response from Device. Please try gain, or check the firmware");
                    }
                }
                else
                {
                    throw new Exception("No Acknowledge response from device. Please try again");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Start Operation on Device Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_RfTrans_Help()
        {
            return true;
        }

        private void Exe_RfTrans_Help()
        {
            NotifyNoFunction();
        }
        #endregion
    }
}
