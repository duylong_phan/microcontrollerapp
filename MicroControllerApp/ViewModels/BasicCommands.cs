﻿using LongModel.ViewModels;
using MicroControllerApp.Models;
using MicroControllerApp.ViewModels.Helpers;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;
using System.Diagnostics;
using Microsoft.Win32;

using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Views.Dialogs;
using LongInterface.Models;
using LongWpfUI.SystemHelper;
using LongWpfUI.Models.Commands;
using MicroControllerApp.Models.ViewClasses.Options;
using LongModel.Collections.MultiChoice;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.ViewClasses.Graphics;
using MicroControllerApp.Models.DataClasses.Packages;
using System.Net;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using LongWpfUI.Windows;
using LongWpfUI.Models.Net;
using LongModel.Helpers;
using LongModel.ViewModels.Logics;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.ViewClasses.Infos;
using MicroControllerApp.Models.DataClasses.ItemInfos;
using System.Text;
using LongModel.Views.Infos.HelpInfos;
using LongInterface.Models.IO.Files;
using LongModel.Models.IO.Files;
using LongModel.ExtensionMethods;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using csmatio.io;
using csmatio.types;

namespace MicroControllerApp.ViewModels
{
    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        #region App
        private bool Can_AboutApp()
        {
            return true;
        }

        private void Exe_AboutApp()
        {
            var window = new AboutWindow(this.View, this.OutputParameter.SoftwareFullName,
                                         this.OutputParameter.AboutDescriptions, this.OutputParameter.AboutFunctions,
                                         this.OutputParameter.AboutReferences, this.OutputParameter.AboutSupports);
            window.ShowDialog();
        }

        private bool Can_CloseApp()
        {
            return true;
        }

        private void Exe_CloseApp()
        {
            this.Dispose();
        }

        #endregion

        #region Configuration
        private bool Can_NewConfig()
        {
            return !this.OutputParameter.IsWorking; //NOT
        }

        private void Exe_NewConfig()
        {
            Configuration config = new Configuration();
            SetViewProperties(config);
            ConfigWindow window = new ConfigWindow(this.View, config, true);
            if(window.ShowDialog() == true)
            {               
                this.configFilePath = string.Empty;
                this.InputParameter.CurrentConfig = config;
                this.OutputParameter.UpdateAppStatus("New configuration was added");
                AssignTaskAction();
                AssignTaskView();

                //Assign View Property for Input Parameter direct in MainWindow
                if (config.CurrentSetting is RfTransSetting)
                {
                    SetViewProperties(config.CurrentSetting);
                }
            }
        }

        private bool Can_LoadConfig(object parameter)
        {
            return !this.OutputParameter.IsWorking; //NOT
        }

        private async void Exe_LoadConfig(object parameter)
        {
            var filePath = parameter as string;
            if (string.IsNullOrEmpty(filePath))
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = App.ConfigFilter;
                dialog.InitialDirectory = App.DataPath;
                if (dialog.ShowDialog() == true)
                {
                    filePath = dialog.FileName;
                }
            }

            try
            {
                if (!string.IsNullOrEmpty(filePath))    //NOT
                {
                    Configuration config = null;
                    await Task.Factory.StartNew(() =>
                    {
                        using (var stream = System.IO.File.OpenRead(filePath))
                        {
                            var serializer = new XmlSerializer(typeof(Configuration));
                            config = serializer.Deserialize(stream) as Configuration;
                        }
                    });

                    if (config != null)
                    {
                        this.configFilePath = filePath;
                        this.InputParameter.CurrentConfig = config;
                        this.OutputParameter.UpdateAppStatus("Configuration file was loaded");
                        var setting = this.InputParameter.CurrentConfig.CurrentSetting;
                        if (setting != null && setting.Port == PhysicalPorts.Serial)
                        {
                            this.RefreshPort.TryToExecute();
                        }                        
                        AssignTaskAction();
                        AssignTaskView();
                        WpfRelayCommand.UpdateCanExecuteStatus();

                        //Assign View Property for Input Parameter direct in MainWindow
                        if (config.CurrentSetting is RfTransSetting)
                        {
                            SetViewProperties(config.CurrentSetting);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load Configuration error", 
                                MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_EditConfig()
        {
            return !this.OutputParameter.IsWorking && this.InputParameter.CurrentConfig != null;    //NOT
        }

        private void Exe_EditConfig()
        {
            SetViewProperties(this.InputParameter.CurrentConfig);
            ConfigWindow window = new ConfigWindow(this.View, this.InputParameter.CurrentConfig, false);
            window.ShowDialog();
            this.OutputParameter.UpdateAppStatus("Configuration file was edited");
            AssignTaskAction();
            AssignTaskView();
        }

        private bool Can_SaveConfig()
        {
            return !this.OutputParameter.IsWorking && this.InputParameter.CurrentConfig != null;    //NOT
        }

        private async void Exe_SaveConfig()
        {
            if (string.IsNullOrEmpty(this.configFilePath))
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = App.ConfigFilter;
                dialog.InitialDirectory = App.DataPath;
                if (dialog.ShowDialog() == true)
                {
                    this.configFilePath = dialog.FileName;
                }
            }

            try
            {
                if (!string.IsNullOrEmpty(this.configFilePath))     //NOT
                {
                    await Task.Factory.StartNew(() =>
                    {
                        using (var stream = System.IO.File.Create(this.configFilePath))
                        {
                            var serializer = new XmlSerializer(typeof(Configuration));
                            serializer.Serialize(stream, this.InputParameter.CurrentConfig);
                        }
                    });
                    this.OutputParameter.UpdateAppStatus("Configuration file was saved");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Save Config Error", 
                                MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_OpenFolder()
        {
            return true;
        }

        private void Exe_OpenFolder()
        {
            try
            {
                if (!System.IO.Directory.Exists(App.DataPath))  //NOT
                {
                    System.IO.Directory.CreateDirectory(App.DataPath);
                }

                Process.Start(App.DataPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Open folder error");
            }
        }

        private bool Can_ShowBasicHelp()
        {
            return true;
        }

        private void Exe_ShowBasicHelp()
        {
            try
            {
                TaskHelpInfo info = null;
                if (this.InputParameter.CurrentConfig != null && this.InputParameter.CurrentConfig.CurrentSetting != null)
                {
                    info = HelpDocFactory.GenerateInfo( HelpTypes.View, this.InputParameter.CurrentConfig.CurrentSetting.TaskID);
                }
                else
                {
                    info = HelpDocFactory.GenerateInfo(HelpTypes.Non);
                }
                var window = new HelpWindow(this.View, info);
                window.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Show Help Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_ShowTaskHelp(object parameter)
        {
            return parameter is MicroTasks;
        }

        private void Exe_ShowTaskHelp(object parameter)
        {
            try
            {
                var task = (MicroTasks)parameter;
                if (task == MicroTasks.Non)
                {
                    throw new Exception("Please select a task");
                }
                var info = HelpDocFactory.GenerateInfo(HelpTypes.Config, task);
                var window = new HelpWindow(this.View, info);
                window.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Show Help Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        #endregion

        #region Operation
        private bool Can_StartOper()
        {
            return this.InputParameter.CurrentConfig != null && !this.OutputParameter.IsWorking;    //NOT
        }

        private async void Exe_StartOper()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting;
            try
            {                
                if (setting == null)
                {
                    throw new Exception("No Task is selected. Please edit configuration");
                }

                #region Initialize Resource
                //Share
                setting.InitializeSW();                

                //Specific
                if (setting.Position == AppPositions.Local)
                {                    
                    //only when Port != WebServer
                    if (setting.ServerSetting.IsEnabled && setting.Port != PhysicalPorts.WebServer)
                    {
                        string prefix = GetServerPrefix(setting.ServerSetting);
                        bool isOk = NetworkHelper.CheckHttpPrefix(prefix);
                        if (!isOk)  //NOT
                        {
                            throw new Exception("Prefix address of Server hasn't been registered");
                        }

                        this.httpServer = new HttpServer();
                        this.httpServer.Start(prefix, setting.OnRequested);
                        this.serverQueue.Clear();
                    }

                    //HW
                    await setting.InitializeHW();
                    WriteOut(RawPackage.Start.GetBytes());
                    await Task.Delay(50);
                }
                else if(setting.Position == AppPositions.Remote)
                {
                    string url = NetworkHelper.GetHttpPrefix(setting.RemoteSetting.Address, setting.RemoteSetting.Port);
                    this.requestWorker = new PeriodWorker();
                    this.requestWorker.SetAction(() =>
                    {
                        return Worker_BackgroundRequest(url, setting.OnResponsed, setting.RemoteSetting.Timeout, setting.RemoteSetting.UpdateInterval);
                    });
                    this.requestWorker.Start();
                }
                #endregion

                #region Update Status
                this.OutputParameter.IsWorking = true;
                this.OutputParameter.UpdateAppStatus("Task operation was started");
                WpfRelayCommand.UpdateCanExecuteStatus();
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Start Operation Error", 
                                MessageBoxButton.OK, MessageBoxImage.Information);
                if (setting != null && setting.Position == AppPositions.Local)
                {
                    await setting.DisposeHW();
                    setting.DisposeSW();
                    //only when server is selected
                    if (this.httpServer != null && this.httpServer.IsEnabled)
                    {
                        this.httpServer.Dispose();
                        this.httpServer = null;
                    }
                }
            }
        }
        
        private bool Can_StopOper()
        {
            return this.InputParameter.CurrentConfig != null && this.OutputParameter.IsWorking;
        }

        private async void Exe_StopOper()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting;
            try
            {
                if (setting.Position == AppPositions.Local)
                {
                    WriteOut(RawPackage.Stop.GetBytes());
                    await Task.Delay(50);
                    //Dispose Resource  
                    await setting.DisposeHW();
                    setting.DisposeSW();
                    //only when server is selected
                    if (this.httpServer != null && this.httpServer.IsEnabled)
                    {
                        this.httpServer.Dispose();
                        this.httpServer = null;
                    }
                }
                else if(setting.Position == AppPositions.Remote && this.requestWorker != null)
                {
                    this.requestWorker.Stop();
                    this.requestWorker = null;
                }                

                //update Status
                this.OutputParameter.IsWorking = false;
                this.OutputParameter.UpdateAppStatus("Task operation was stopped");
                WpfRelayCommand.UpdateCanExecuteStatus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Start Operation Error",
                                MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_ReadMasterSetting()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_ReadMasterSetting()
        {
            try
            {
                var package = RawPackage.ReadMasterSetting;

                this.hasResponse = false;
                WriteOut(package);
                await Task.Delay(WaitResponse);

                if(this.hasResponse && this.lastResponse is MasterSettingPackage)
                {
                    this.OutputParameter.UpdateAppStatus("Master setting is received");
                    var settingPackage = this.lastResponse as MasterSettingPackage;
                    SetViewProperties(settingPackage);
                    var window = new DeviceSettingEditorWindow(this.View, settingPackage, MicroTasks.Non);
                    window.ShowDialog();
                }
                else
                {
                    throw new Exception("No response from device. Please check connection or firmware!!!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Read master setting error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_WriteMasterSetting(object parameter)
        {
            return this.OutputParameter.IsWorking && parameter is MasterSettingPackage;
        }

        private async void Exe_WriteMasterSetting(object parameter)
        {
            try
            {
                var isOk = false;
                var package = parameter as MasterSettingPackage;

                //Check input parameter
                var window = package.View as DeviceSettingEditorWindow;
                string errorMessage = string.Empty;
                if(window.HasError(out errorMessage))
                {
                    throw new Exception(errorMessage);
                }
                //Get Buffer
                var buffer = package.GetWriteBytes();
                
                //Write Buffer
                this.hasResponse = false;
                WriteOut(buffer);
                await Task.Delay(WaitResponse);

                //Check response
                if (this.hasResponse && this.lastResponse is CommandDonePackage)
                {
                    var donePackage = this.lastResponse as CommandDonePackage;
                    if (donePackage.Command == PcCommands.Write_MasterSetting)
                    {
                        this.OutputParameter.UpdateAppStatus("Master device setting was updated");
                        this.StopOper.TryToExecute();
                        isOk = true;
                    }
                }

                //Notify error
                if (!isOk)  //NOT
                {
                    throw new Exception("Update Master device setting failed: No response");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Write master setting error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion

        #region OutPin

        private bool Can_OutPin_ResetPin(object parameter)
        {
            return this.OutputParameter.IsWorking && parameter is OutPinHandlerSetting;
        }

        private void Exe_OutPin_ResetPin(object parameter)
        {
            try
            {
                var setting = parameter as OutPinHandlerSetting;
                if (setting.Command == OutPinCommands.PWM_Auto || 
                    setting.Command == OutPinCommands.PWM_Servo_Auto ||
                    setting.Command == OutPinCommands.PWM_Servo_SV1270TG_Auto)
                {
                    //dispose resource
                    setting.IsFuncEnabled = false;
                }
                var package = new RawPackage((byte)PcCommands.PinWrite, setting.GetBytes(OutPinCommands.Reset));
                WriteOut(package);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Reset Out Pin Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_OutPin_SetPin(object parameter)
        {
            return this.OutputParameter.IsWorking && parameter is OutPinHandlerSetting;
        }

        private void Exe_OutPin_SetPin(object parameter)
        {
            try
            {
                var setting = parameter as OutPinHandlerSetting;
                var package = new RawPackage((byte)PcCommands.PinWrite, setting.GetBytes(OutPinCommands.Set));
                WriteOut(package);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Set Out Pin Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_OutPin_ExeCommand(object parameter)
        {
            return this.OutputParameter.IsWorking && parameter is OutPinHandlerSetting;
        }

        private void Exe_OutPin_ExeCommand(object parameter)
        {
            try
            {
                var setting = parameter as OutPinHandlerSetting;
                if (setting.Command == OutPinCommands.PWM_Auto || setting.Command == OutPinCommands.PWM_Servo_Auto ||
                    setting.Command == OutPinCommands.PWM_Servo_SV1270TG_Auto)
                {
                    var errorMessage = string.Empty;

                    if (setting.Command == OutPinCommands.PWM_Auto)
                    {
                        foreach (var item in setting.SelectedVariable.Array[0])
                        {
                            if (item < 0 || item > OutPinHandlerSetting.MaxPercent)
                            {
                                errorMessage = string.Format("Value in Input Source has value large than {0}, or smaller than 0", OutPinHandlerSetting.MaxPercent);
                                break;
                            }
                        }
                    }
                    else if (setting.Command == OutPinCommands.PWM_Servo_Auto)
                    {
                        foreach (var item in setting.SelectedVariable.Array[0])
                        {
                            if (item < -OutPinHandlerSetting.MaxPercent || 
                                item > OutPinHandlerSetting.MaxPercent)
                            {
                                errorMessage = string.Format("Value in Input Source has value large than {0}, or smaller than -{0}", OutPinHandlerSetting.MaxPercent);
                                break;
                            }
                        }
                    }
                    else if(setting.Command == OutPinCommands.PWM_Servo_SV1270TG_Auto)
                    {
                        foreach (var item in setting.SelectedVariable.Array[0])
                        {
                            if (item < -OutPinHandlerSetting.MaxDegree_SV1270TG || 
                                item > OutPinHandlerSetting.MaxDegree_SV1270TG)
                            {
                                errorMessage = string.Format("Value in Input Source has value large than {0}, or smaller than -{0}", OutPinHandlerSetting.MaxDegree_SV1270TG);
                                break;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(errorMessage))
                    {
                        setting.ContentIndex = 0;
                        setting.AutoProgressPercent = 0;
                        setting.IsFuncEnabled = true;
                    }
                    else
                    {
                        throw new Exception(errorMessage);
                    }
                }
                else
                {
                    var package = new RawPackage((byte)PcCommands.PinWrite, setting.GetBytes());
                    WriteOut(package);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Execute Command on Out Pin Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_OutPin_LoadFile(object parameter)
        {
            return this.OutputParameter.IsWorking && parameter is OutPinHandlerSetting;
        }

        private async void Exe_OutPin_LoadFile(object parameter)
        {
            try
            {
                var setting = parameter as OutPinHandlerSetting;
                this.SelectFile.TryToExecute(setting);
                if (!string.IsNullOrEmpty(setting.FilePath)) //NOT
                {
                    var collection = new List<DoubleMatVariableInfo>();
                    await Task.Factory.StartNew(() =>
                    {
                        var reader = new MatFileReader(setting.FilePath);
                        foreach (var key in reader.Content.Keys)
                        {
                            if (reader.Content[key].IsDouble && reader.Content[key].Dimensions != null && 
                                reader.Content[key].Dimensions.Length >= 2 && reader.Content[key].Dimensions[0] == 1)
                            {
                                var content = reader.Content[key] as MLDouble;
                                collection.Add(new DoubleMatVariableInfo(content.Name, content.GetArray()));
                            }
                        }
                    });

                    setting.InputVariables.Clear();
                    foreach (var item in collection)
                    {
                        setting.InputVariables.Add(item);
                    }
                    if (setting.InputVariables.Count > 0)
                    {
                        setting.SelectedVariable = setting.InputVariables[0];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load file Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        
        private bool Can_OutPin_SendAngleServor()
        {
            return this.OutputParameter.IsWorking;
        }

        private void Exe_OutPin_SendAngleServor()
        {
            try
            {
                double angle = 0;
                switch (View.GetAngleServoOption())
                {
                    case 0:
                        if (this.InputParameter.SelectedOutPin.SelectedVariable == null)
                        {
                            throw new Exception("No Matlab Variable is available");
                        }

                        //reset when all value has been use
                        if (this.InputParameter.SelectedOutPin.ContentIndex >= this.InputParameter.SelectedOutPin.SelectedVariable.Array[0].Length)
                        {
                            this.InputParameter.SelectedOutPin.ContentIndex = 0;
                        }
                        //take value
                        angle = this.InputParameter.SelectedOutPin.SelectedVariable.Array[0][this.InputParameter.SelectedOutPin.ContentIndex];
                        this.InputParameter.SelectedOutPin.ContentIndex++;
                        //update UI
                        this.InputParameter.SelectedOutPin.AutoProgressPercent = this.InputParameter.SelectedOutPin.ContentIndex * 100 / this.InputParameter.SelectedOutPin.SelectedVariable.Array[0].Length;
                        break;

                    case 1:
                    default:
                        var isOK = View.GetManualAngleServo(out angle);
                        //invalid input from user
                        if (!isOK)  //NOT
                        {
                            return;
                        }
                        break;
                }

                if (angle > 85 || angle < -85)
                {
                    throw new Exception("Value should be in range of [-85;85]");
                }

                ushort rawAngle = (ushort)(OutPinHandlerSetting.MaxDegree_SV1270TG + angle);
                rawAngle = (ushort)(rawAngle * 100.0 / OutPinHandlerSetting.MaxDegree_SV1270TG);
                this.InputParameter.SelectedOutPin.ServoPercent = rawAngle;
                var package = new RawPackage((byte)PcCommands.PinWrite, this.InputParameter.SelectedOutPin.GetBytes());
                WriteOut(package);

                this.OutputParameter.UpdateAppStatus("Value was sent");
            }
            catch (Exception ex)
            {
                this.OutputParameter.UpdateAppStatus(ex.Message, AppStatusTypes.Error);
            }
        }
        #endregion

        #region File View       

        private bool Can_LoadDataFile(object parameter)
        {
            return true;
        }

        private void Exe_LoadDataFile(object parameter)
        {
            try
            {
                string filePath = parameter as string;
                if (string.IsNullOrEmpty(filePath))
                {
                    var dialog = new OpenFileDialog();
                    if (dialog.ShowDialog() == true)
                    {
                        filePath = dialog.FileName;
                    }
                    else
                    {
                        return;
                    }
                }

                var extension = System.IO.Path.GetExtension(filePath);
                FileContentInfo fileContent = null;
                switch (extension)
                {
                    case PressSetting.PressExtension:                        
                        using (var stream = System.IO.File.OpenRead(filePath))
                        {
                            var name = System.IO.Path.GetFileName(filePath);
                            fileContent = Press_GetFileContent(stream, name, true);
                        }
                        break;

                    default:
                        break;
                }

                if (fileContent != null)
                {
                    //Explicit select File Tab
                    this.View.UpdateRibbonTabIndex(MainWindow.Index_FileTab);
                    //Add Page to View
                    this.OutputParameter.DocumentPages.Add(fileContent);
                    //Notify User
                    this.OutputParameter.UpdateAppStatus("A file was loaded");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load File Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_CloseFilePage(object parameter)
        {
            return parameter is FileContentInfo;
        }

        private void Exe_CloseFilePage(object parameter)
        {
            try
            {
                var info = parameter as FileContentInfo;
                this.OutputParameter.DocumentPages.Remove(info);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Close Page error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_SaveFilePage(object parameter)
        {
            return parameter is FileContentInfo;
        }

        private void Exe_SaveFilePage(object parameter)
        {
            try
            {
                var info = parameter as FileContentInfo;
                var dialog = new SaveFileDialog();

                #region Set Name, Filter...
                switch (info.Extension)
                {
                    case FileContentExtensions.press:
                        dialog.FileName = info.Header as string;
                        dialog.Filter = PressSetting.PressFilter;
                        break;
                    default:
                        break;
                }
                #endregion

                #region Save Action
                if (dialog.ShowDialog() == true)
                {
                    var message = string.Empty;
                    switch (info.Extension)
                    {
                        case FileContentExtensions.press:
                            using (var stream = System.IO.File.Create(dialog.FileName))
                            {
                                this.pressFileHandler.WriteFile(stream, info.Content as List<PressInfo>);
                            }
                            message = "Press Pliers file was saved successfully";
                            break;



                        default:
                            throw new NotImplementedException("Extension is not supported by SaveFilePage Command: " + info.Extension);
                    }

                    //Notify User
                    if (!string.IsNullOrEmpty(message)) //NOT
                    {
                        this.OutputParameter.UpdateAppStatus(message);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Save file error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        #endregion 
        
        #region Share
        private bool Can_RefreshPort()
        {
            return true;
        }

        private void Exe_RefreshPort()
        {
            try
            {
                string[] ports = SerialPort.GetPortNames();
                this.InputParameter.PortCollection.Clear();

                foreach (var item in ports)
                {
                    this.InputParameter.PortCollection.Add(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Get Serial Port error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
                
        private bool Can_SelectDirectory(object parameter)
        {
            return parameter is IDirectory;
        }

        private void Exe_SelectDirectory(object parameter)
        {
            var data = parameter as IDirectory;
            if (data != null)
            {
                System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    bool isEditable = FileFolderHelper.IsDirectoryEditable(dialog.SelectedPath);
                    if (isEditable)
                    {
                        data.Directory = dialog.SelectedPath;
                    }
                    else
                    {
                        MessageBox.Show("Selected Directory cannot be written oder read. Please choose another Directory", "Invalid Directory", 
                                        MessageBoxButton.OK, MessageBoxImage.Information);
                    }                    
                }
            } 
        }

        private bool Can_SelectFile(object parameter)
        {
            return parameter is IFilePath;
        }

        private void Exe_SelectFile(object parameter)
        {
            try
            {
                var data = parameter as IFilePath;
                var dialog = new OpenFileDialog();
                if (data.Tag is string)
                {
                    dialog.Filter = data.Tag as string;
                }

                if (dialog.ShowDialog() == true)
                {
                    data.FilePath = dialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Select file Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #region USB
        private bool Can_DisableDeviceSignature()
        {
            return true;
        }

        private void Exe_DisableDeviceSignature()
        {
            try
            {
                UsbHelper.SetDeviceSignatureStatus(false);
                MessageBox.Show("DeviceSignature is disabled successfully. Please restart the computer", "Disable Device Signature");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Disable Device Signature Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        
        private bool Can_EnableDeviceSignature()
        {
            return true;
        }

        private void Exe_EnableDeviceSignature()
        {
            try
            {
                UsbHelper.SetDeviceSignatureStatus(true);
                MessageBox.Show("DeviceSignature is enabled successfully. Please restart the computer", "Enable Device Signature");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Enable Device Signature Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        #endregion

        #region Server/Remote

        private bool Can_GetIpAddress(object parameter)
        {
            return parameter is ServerSetting;
        }

        private void Exe_GetIpAddress(object parameter)
        {
            try
            {
                var address = GetLocalIPAddress();
                MessageBox.Show("Local IP Address is: " + address);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Get IP address error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_RegisterPort(object parameter)
        {
            return parameter is ServerSetting;
        }

        private async void Exe_RegisterPort(object parameter)
        {
            int code = 0;
            try
            {
                var setting = parameter as ServerSetting;  
                ProgressingWindow.ShowDialog(this.View, "Registering port on Firewall");
                await Task.Factory.StartNew(() =>
                {
                    Task.Delay(100).Wait();
                    var info = new ProcessStartInfo();
                    info.Verb = "runas";
                    info.FileName = App.FireWallHelperPath;
                    info.Arguments = string.Format("Port \"{0}\" {1} TCP In True", App.SoftwareName, setting.Port);
                    var process = Process.Start(info);
                    process.WaitForExit();
                    code = process.ExitCode;                    
                });                
            }
            catch (Exception)
            {
                code = 0;
            }
            ProgressingWindow.CloseDialog();

            string message = string.Empty;
            switch (code)
            {
                case 1:     message = "Input argument is invalid";              break;
                case 2:     message = "Port registration is successful";        break;
                case 3:     message = "Port is existed. No need to register";   break;
                case 0:
                default:    message = "There is an error";                      break;
            }
            MessageBox.Show(message, "Port registration");
        }
                
        private bool Can_RegisterPrefix(object parameter)
        {
            return parameter is ServerSetting;
        }

        private void Exe_RegisterPrefix(object parameter)
        {
            try
            {                
                string prefix = GetServerPrefix(parameter as ServerSetting);
                NetworkHelper.RegisterPrefix(prefix);
                MessageBox.Show("Prefix address has been registered successfully", "Prefix registration", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Register prefix error", MessageBoxButton.OK, MessageBoxImage.Information); 
            }
        }        

        private bool Can_CheckServer(object parameter)
        {
            return parameter is RemoteSetting;
        }

        private async void Exe_CheckServer(object parameter)
        {
            var setting = parameter as RemoteSetting;
            string url = NetworkHelper.GetHttpPrefix(setting.Address, setting.Port);
            try
            {
                var message = string.Empty;    
                await Task.Factory.StartNew(() =>
                {
                    Action<string, byte[]> action = (_suffix, _buffer) =>
                    {
                        message = Encoding.UTF8.GetString(_buffer);
                    };
                    WebHelper.Request(url, WebHelper.Req_Ping, action, setting.Timeout);
                });

                if (string.IsNullOrEmpty(message))
                {
                    throw new Exception("No response from server");
                }
                MessageBox.Show("Server Information: " + message, "Ping server", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }
                MessageBox.Show(ex.Message, "Ping server error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        
        #endregion

        #region Graphic
        private bool Can_OpenCurveSetting(object parameter)
        {
            return parameter is CurveInfo;
        }

        private void Exe_OpenCurveSetting(object parameter)
        {
            try
            {
                var window = new CurveSettingWindow(this.View, parameter as CurveInfo);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Edit Curve error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        
        private bool Can_OpenAxisSetting(object parameter)
        {
            return parameter is AxisInfo;
        }

        private void Exe_OpenAxisSetting(object parameter)
        {
            try
            {
                var window = new AxisSettingWindow(this.View, parameter as AxisInfo);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Edit Axis error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        #endregion

        #endregion

        #region Private Method

        private void AssignTaskAction()
        {
            //Task is available
            if (this.InputParameter.CurrentConfig != null && this.InputParameter.CurrentConfig.CurrentSetting != null)
            {
                var setting = this.InputParameter.CurrentConfig.CurrentSetting;
                switch (setting.TaskID)
                {
                    case MicroTasks.Press:
                        setting.SetLocalAction(Press_InitializeSW, Press_InitializeHW, Press_Update, Press_DisposeSW, Press_DisposeHW);
                        setting.SetRemoteAction(Press_OnRequested, Press_OnResponsed);
                        break;

                    case MicroTasks.Capacitor:
                        setting.SetLocalAction(Cap_InitializeSW, Cap_InitializeHW, Cap_Update, Cap_DisposeSW, Cap_DisposeHW);
                        setting.SetRemoteAction(Cap_OnRequested, Cap_OnResponsed);
                        break;

                    case MicroTasks.Acceleration:
                        setting.SetLocalAction(Acc_InitializeSW, Acc_InitializeHW, Acc_Update, Acc_DisposeSW, Acc_DisposeHW);
                        setting.SetRemoteAction(Acc_OnRequested, Acc_OnResponsed);
                        break;

                    case MicroTasks.uBalance:
                        setting.SetLocalAction(uBalance_InitializeSW, uBalance_InitializeHW, uBalance_Update, uBalance_DisposeSW, uBalance_DisposeHW);
                        setting.SetRemoteAction(uBalance_OnRequested, uBalance_OnResponsed);
                        break;

                    case MicroTasks.RfTrans:
                        setting.SetLocalAction(RfTrans_InitializeSW, RfTrans_InitializeHW, RfTrans_Update, RfTrans_DisposeSW, RfTrans_DisposeHW);
                        setting.SetRemoteAction(RfTrans_OnRequested, RfTrans_OnResponsed);
                        break;

                    case MicroTasks.MotorCtr:
                        setting.SetLocalAction(MotorCtr_InitializeSW, MotorCtr_InitializeHW, MotorCtr_Update, MotorCtr_DisposeSW, MotorCtr_DisposeHW);
                        setting.SetRemoteAction(MotorCtr_OnRequested, MotorCtr_OnResponsed);
                        var motorCtrSetting = setting as MotorCtrSetting;
                        motorCtrSetting.SetFileChangedAction(MotorCtr_OnFileChanged);
                        break;

                    case MicroTasks.Non:
                    default:
                        //Ignore
                        break;
                }
            }
        }

        private void AssignTaskView()
        {
            //Task is available
            if (this.InputParameter.CurrentConfig != null && this.InputParameter.CurrentConfig.CurrentSetting != null)
            {
                var setting = this.InputParameter.CurrentConfig.CurrentSetting;

                #region Add ViewOptions
                var collection = new List<ViewOption>();
                collection.Add(new ViewOption("No View", AppViews.Non));
                switch (setting.TaskID)
                {
                    case MicroTasks.Press:
                        var pressSetting = setting as PressSetting;
                        collection.Add(new ViewOption("Graphic", AppViews.Graphic) { Content = this.OutputParameter.PlotModel });
                        collection.Add(new ViewOption("Closed Pliers Table", AppViews.PressTable) { Content = this.OutputParameter.PressCollection });
                        if (pressSetting.Position == AppPositions.Local)
                        {
                            collection.Add(new ViewOption("Control Panel", AppViews.PressLocalControl) { Content = this });
                        }
                        else if (pressSetting.Position == AppPositions.Remote)
                        {
                            collection.Add(new ViewOption("Control Panel", AppViews.PressRemoteControl) { Content = this });
                        }
                        break;

                    case MicroTasks.Capacitor:
                        var capSetting = setting as CapSetting;
                        collection.Add(new ViewOption("Graphic", AppViews.Graphic) { Content = this.OutputParameter.PlotModel });
                        if (capSetting.Position == AppPositions.Local)
                        {
                            collection.Add(new ViewOption("Control Panel", AppViews.CapControl) { Content = this });
                        }
                        break;

                    case MicroTasks.Acceleration:
                        var accSetting = setting as AccSetting;
                        collection.Add(new ViewOption("Graphic", AppViews.Graphic) { Content = this.OutputParameter.PlotModel });
                        if (accSetting.Position == AppPositions.Local)
                        {
                            collection.Add(new ViewOption("Control Panel", AppViews.AccControl) { Content = this });
                        }
                        break;

                    case MicroTasks.uBalance:
                        var uBalanceSetting = setting as uBalanceSetting;
                        collection.Add(new ViewOption("Action Editor", AppViews.uBalanceEditor) { Content = this.OutputParameter.uBalanceContainers });
                        collection.Add(new ViewOption("Control Panel", AppViews.uBalanceControl) { Content = this });

                        foreach (var item in this.OutputParameter.uBalanceContainers)
                        {
                            item.AddAction = this.uBalance_AddAction;
                            item.EditAction = this.uBalance_EditAction;
                            item.RemoveAction = this.uBalance_RemoveAction;
                            item.UpAction = this.uBalance_UpAction;
                            item.DownAction = this.uBalance_DownAction;
                        }
                        break;

                    case MicroTasks.RfTrans:
                        var rfTransSetting = setting as RfTransSetting;
                        collection.Add(new ViewOption("Graphic", AppViews.Graphic) { Content = this.OutputParameter.PlotModel });
                        collection.Add(new ViewOption("Control Panel", AppViews.RfTransControl) { Content = this });
                        break;

                    case MicroTasks.MotorCtr:
                        var motorCtrSetting = setting as MotorCtrSetting;
                        collection.Add(new ViewOption("Control Panel", AppViews.MotorCtrContol) { Content = this });
                        collection.Add(new ViewOption("Package Editor", AppViews.MotorCtrEditor) { Content = this.OutputParameter.MotorCtrContainer });                        
                        collection.Add(new ViewOption("Graphic", AppViews.Graphic) { Content = this.OutputParameter.PlotModel });
                        collection.Add(new ViewOption("Output Pin Control Panel", AppViews.OutPinCtr) { Content = this.InputParameter });

                        foreach (var item in this.OutputParameter.MotorCtrContainer)
                        {
                            item.AddAction = this.MotorCtr_AddAction;
                            item.EditAction = this.MotorCtr_EditAction;
                            item.RemoveAction = this.MotorCtr_RemoveAction;
                            item.UpAction = this.MotorCtr_UpAction;
                            item.DownAction = this.MotorCtr_DownAction;
                        }
                        break;

                    case MicroTasks.Non:
                    default:
                        break;
                }
                #endregion

                #region Create, Assign ViewOption
                var multiView = new MultiChoice2Option<ViewOption>(collection, collection[0]);
                foreach (var item in multiView.Collection1)
                {
                    if (item.Type == setting.LeftView)
                    {
                        multiView.SelectedItem1 = item;
                        break;
                    }
                }
                foreach (var item in multiView.Collection2)
                {
                    if (item.Type == setting.RightView)
                    {
                        multiView.SelectedItem2 = item;
                        break;
                    }
                }
                #endregion

                #region Update UI
                this.OutputParameter.ViewCollection = multiView;
                this.View.SetViewPanelStatus(true);
                #endregion               

                #region Interface Support
                if (setting is IGraphic)
                {
                    var graphic = setting as IGraphic;
                    //Property
                    this.OutputParameter.PlotModel.CopyProperties(graphic.GraphicSetting);

                    //Axis    
                    this.OutputParameter.PlotModel.Axes.Clear();
                    foreach (var item in graphic.GraphicSetting.AxisCollection)
                    {
                        var axis = new DataAxis();
                        axis.CopyProperties(item);
                        this.OutputParameter.PlotModel.Axes.Add(axis);
                    }

                    //Curve   
                    this.OutputParameter.PlotModel.Series.Clear();
                    foreach (var item in graphic.GraphicSetting.CurveCollection)
                    {
                        var curve = new DataCurve();
                        curve.CopyProperties(item);
                        this.OutputParameter.PlotModel.Series.Add(curve);
                    }

                    switch (setting.TaskID)
                    {
                        case MicroTasks.Acceleration:
                            int index = 0;
                            this.OutputParameter.AxisStatus1.SetAxis(this.OutputParameter.PlotModel.Series[index],
                                                                     this.OutputParameter.PlotModel.Series[index + 1],
                                                                     this.OutputParameter.PlotModel.Series[index + 2]);
                            index += 3;
                            this.OutputParameter.AxisStatus2.SetAxis(this.OutputParameter.PlotModel.Series[index],
                                                                     this.OutputParameter.PlotModel.Series[index + 1],
                                                                     this.OutputParameter.PlotModel.Series[index + 2]);
                            index += 3;
                            this.OutputParameter.AxisStatus3.SetAxis(this.OutputParameter.PlotModel.Series[index],
                                                                     this.OutputParameter.PlotModel.Series[index + 1],
                                                                     this.OutputParameter.PlotModel.Series[index + 2]);
                            index += 3;
                            this.OutputParameter.AxisStatus4.SetAxis(this.OutputParameter.PlotModel.Series[index],
                                                                     this.OutputParameter.PlotModel.Series[index + 1],
                                                                     this.OutputParameter.PlotModel.Series[index + 2]);                            
                            break;

                        case MicroTasks.Non:
                        case MicroTasks.Press:
                        case MicroTasks.Capacitor:
                        case MicroTasks.uBalance:
                        case MicroTasks.RfTrans:
                        case MicroTasks.MotorCtr:
                        default:
                            break;
                    }

                    this.OutputParameter.UpdateGraphic();
                }
                #endregion
            }
        }

        private void FileWatcher_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            //to sort and avoid Append, Edit, or Create
            this.fileChangedIndex++;
            if ((this.fileChangedIndex % this.fileChangedOffset) != 0)
            {
                return;
            }

            if (this.InputParameter.CurrentConfig == null)
            {
                return;
            }

            //make sure the other Software releases File
            Task.Delay(100).Wait();

            try
            {
                //With Setting Support
                if (this.InputParameter.CurrentConfig.CurrentSetting is INotifyFileChanged<FileChangedSetting>)
                {
                    var setting = this.InputParameter.CurrentConfig.CurrentSetting as INotifyFileChanged<FileChangedSetting>;
                    string input = null;
                    using (var stream = System.IO.File.Open(e.FullPath, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite))
                    {
                        switch (setting.FileChangedSetting.Response)
                        {
                            case FileChangedResponses.XmlElement:
                                using (var reader = new System.IO.StreamReader(stream))
                                {
                                    input = reader.ReadXmlElement(setting.FileChangedSetting.XmlTag);
                                }
                                break;

                            case FileChangedResponses.FixLength:
                                var buffer = new byte[setting.FileChangedSetting.Length];
                                stream.ReadReverse(buffer, 0, buffer.Length);
                                input = Encoding.UTF8.GetString(buffer);
                                break;

                            case FileChangedResponses.CustomedAction:
                                throw new Exception("Action on FileChanged is on supported");

                            case FileChangedResponses.NewLine:
                            default:
                                input = stream.ReadLineReverse(true);
                                break;
                        }
                    }
                    setting.OnFileChanged(input);
                }
                //No Setting => Muss be self implemented
                else if (this.InputParameter.CurrentConfig.CurrentSetting is INotifyFileChanged)
                {
                    var setting = this.InputParameter.CurrentConfig.CurrentSetting as INotifyFileChanged;
                    setting.OnFileChanged(e.FullPath);
                }
            }
            catch (Exception ex)
            {
                App.Current.Dispatcher.Invoke(() =>
                {
                    if (this.fileWatcher != null)
                    {
                        this.fileWatcher.EnableRaisingEvents = false;
                        this.fileWatcher.Dispose();
                        this.fileWatcher = null;
                    }
                    this.OutputParameter.UpdateAppStatus(ex.Message, AppStatusTypes.Error);
                });
            }
        }

        private bool Worker_BackgroundRequest(string url, Action<string, byte[]> action, int timeout, int updateInterval)
        {
            try
            {
                WebHelper.Request(url, string.Empty, action, timeout);
                Task.Delay(updateInterval).Wait();     
            }
            catch (Exception)
            {
                App.Current.Dispatcher.Invoke(() =>
                {
                    this.StopOper.TryToExecute();
                    MessageBox.Show("Server is not available. Please check server", "No server response", 
                                    MessageBoxButton.OK, MessageBoxImage.Information);
                });
            }        
            return false;
        }

        private string GetLocalIPAddress()
        {
            string address = NetworkHelper.GetIpV4();

            if (string.IsNullOrEmpty(address))
            {
                throw new Exception("Computer is not connected to any IP Network");
            }

            return address;
        }

        private string GetServerPrefix(ServerSetting setting)
        {
            var address = GetLocalIPAddress();
            var prefix = NetworkHelper.GetHttpPrefix(address, setting.Port);
            return prefix;
        }
        #endregion
    }
}
