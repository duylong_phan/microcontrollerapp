﻿using MicroControllerApp.Models.DataClasses.Settings.Sub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.ViewModels.Logics
{
    public enum MotorAutoMethods
    {
        SamePos,
        Motor1Pos,
        Motor1Neg,
        Motor2Pos,
        Motor2Neg
    }

    public class MotorUnbalanceController
    {
        public const double NewPulsePercent = 0.8;

        #region Getter            
        public MotorUnbalanceSetting Setting { get; private set; }        
        public bool IsOn { get; private set; }                
        #endregion

        #region Getter, Setter
        public bool CanInvert { get; set; }
        public MotorAutoMethods Method { get; set; }
        public double TolerancePercent { get; set; }
        public ushort Pulse { get; set; }        
        #endregion

        #region Field    
        private double lastValue;
        private bool hasValue;
        #endregion

        #region Constructor
        public MotorUnbalanceController(MotorUnbalanceSetting setting)
        {
            this.lastValue = double.MaxValue;

            this.Setting = setting;            
            this.Pulse = (ushort)setting.PulseAmount;
            this.TolerancePercent = setting.Tolerance;          
            this.Method = MotorAutoMethods.SamePos;
            this.CanInvert = false;
            this.IsOn = false;            
        }
        #endregion

        #region Public Method
        public void Calculate(double unbalace, double angle)
        {
            if (!this.hasValue) //NOT
            {
                this.hasValue = true;
                this.lastValue = unbalace;
                return;
            }

            if (unbalace < this.Setting.DesiredValue)
            {
                this.IsOn = false;
            }
            else
            {
                var tolerant = this.lastValue * this.TolerancePercent;
                this.IsOn = true;

                if (unbalace > (this.lastValue + tolerant))
                {
                    //get to last position and use next method
                    this.CanInvert = true;
                }
            }
            this.lastValue = unbalace;
        }        
        #endregion
    }
}
