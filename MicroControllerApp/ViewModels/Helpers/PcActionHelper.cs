﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.ViewModels.Helpers
{
    public static class PcActionHelper
    {
        #region Field
        private static Dictionary<uint, Action<object>> actionDict;
        #endregion

        #region Static Constructor
        static PcActionHelper()
        {
            actionDict = new Dictionary<uint, Action<object>>();
        }
        #endregion

        #region Public Method
        public static void AddAction(uint key, Action<object> action)
        {
            if (actionDict.ContainsKey(key))
            {
                actionDict[key] = action;
            }
            else
            {
                actionDict.Add(key, action);
            }            
        }

        public static void ClearActions()
        {
            actionDict.Clear();
        }

        public static void DoAction(uint code, object parameter, bool onlyFirst = false)
        {
            foreach (var key in actionDict.Keys)
            {
                if ((code & key) != 0 && actionDict[key] != null)
                {
                    actionDict[key](parameter);
                    if (onlyFirst)
                    {
                        break;
                    }
                }
            }
        }
        #endregion
    }
}
