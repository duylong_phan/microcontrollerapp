﻿using LongModel.Views.Infos.HelpInfos;
using MicroControllerApp.Models.DataClasses.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.ExtensionMethods;
using MicroControllerApp.Resources;
using LongModel.Views.Infos;
using LongInterface.Views.Infos;

namespace MicroControllerApp.ViewModels.Helpers
{
    public enum HelpTypes
    {
        Non,
        Config,
        View
    }
    
    public static class HelpDocFactory
    { 
        public static TaskHelpInfo GenerateInfo(HelpTypes type, object parameter = null)
        {
            TaskHelpInfo info = null;

            #region Generate
            if (parameter is MicroTasks)
            {
                var task = (MicroTasks)parameter;

                switch (type)
                {
                    case HelpTypes.Config:
                        switch (task)
                        {
                            case MicroTasks.Press:          info = Generate_Config_Press();         break;
                            case MicroTasks.Capacitor:      info = Generate_Config_Capacitor();     break;
                            case MicroTasks.Acceleration:   info = Generate_Config_Acceleration();  break;
                            case MicroTasks.uBalance:       info = Generate_Config_uBalance();      break;
                            case MicroTasks.RfTrans:        info = Generate_Config_RfTrans();       break;
                            case MicroTasks.MotorCtr:       info = Generate_Config_MotorCtr();      break;
                            case MicroTasks.Non:
                            default:                        /*Ignore*/                              break;
                        }
                        break;


                    case HelpTypes.View:   
                        switch (task)
                        {
                            case MicroTasks.Press:          info = Generate_View_Press();           break;
                            case MicroTasks.Capacitor:      info = Generate_View_Capacitor();       break;
                            case MicroTasks.Acceleration:   info = Generate_View_Acceleration();    break;
                            case MicroTasks.uBalance:       info = Generate_View_uBalance();        break;
                            case MicroTasks.RfTrans:        info = Generate_View_RfTrans();         break;
                            case MicroTasks.MotorCtr:       info = Generate_View_MotorCtr();        break;
                            case MicroTasks.Non:
                            default:                        /*Ignore*/                              break;
                        }
                        break;

                    case HelpTypes.Non:
                    default:                                /*Ignore*/                              break;
                }                
            }
            #endregion

            #region Support Basic Help ?
            switch (type)
            {   
                case HelpTypes.Config:
                    //ignore
                    break;

                    //Add Basic Help
                case HelpTypes.View:
                    var tmp = info;
                    info = Generate_SoftwareGuild(info.Header);
                    info.SectionInfos.AddRange(tmp.SectionInfos);
                    break;

                    //Basic Help
                case HelpTypes.Non:
                default:
                    info = Generate_SoftwareGuild("Basic Software Help");
                    break;
            }
            #endregion

            return info;
        }

        #region Common Help
        private static ImageContentHelpInfo GetDeviceManager()
        {
            var imageHelp = new ImageContentHelpInfo("Open Device manager to check if driver is correctly installed", IconAccessor.Help_DeviceManager.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Right Mouse click on My Computer"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Select Manage"));
            return imageHelp;
        }

        private static ImageContentHelpInfo GetSerialPort()
        {
            var imageHelp = new ImageContentHelpInfo("Serial Communication", IconAccessor.Help_Communication1.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Type of physical communication Port"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Port name from Windows OS"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Communication speed"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Amount of bit per data"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(5, "Check error Type"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(6, "No Communication detection"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(7, "Update per bytes"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(8, "Activate DTR in communication"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(9, "Activate RTS in communication"));
            imageHelp.Notes.Add("Click the \"Refresh\" Image to update list of available Ports, make sure the driver of the device is correctly installed.");
            imageHelp.Notes.Add("Only \"Port\" should be edited, other parameters should only be changed with notification from supervisor");

            return imageHelp;
        }

        private static ImageContentHelpInfo GetUsbPort()
        {
            var imageHelp = new ImageContentHelpInfo("USB Communication", IconAccessor.Help_Communication2.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Type of physical communication Port"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "ID of device manufacture"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "ID of device"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Enable Device Driver Signature"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(5, "Disable Device Driver Signature"));
            imageHelp.Notes.Add("Please make sure the device driver is correctly installed");
            imageHelp.Notes.Add("All parameters should be constant. They should be changed only with notification from supervisor");

            return imageHelp;
        }

        private static ImageContentHelpInfo GetWebServer()
        {
            var imageHelp = new ImageContentHelpInfo("Web Server", IconAccessor.Help_Communication3.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Activate Server"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Input, Output communication port"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Get and notify the current IP Address"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Check, register Port in Windows OS Firewall"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(5, "Check, register Server address in Windows OS Web Service"));
            imageHelp.Notes.Add("This feature is available alone, or combine with Serial/USB");
            imageHelp.Notes.Add("Please make sure the computer connects to a IP Network");
            imageHelp.Notes.Add("Port and Prefix have to be registered, in order for the server to work probably");

            return imageHelp;
        }

        private static ImageContentHelpInfo GetWebClient()
        {
            var imageHelp = new ImageContentHelpInfo("Web Client", IconAccessor.Help_Communication4.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Input, Output communication port"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Client Name"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "IP Address of Server"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Communication Timeout"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(5, "Time interval for communition request"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(6, "Check if server is available, with given Parameters"));
            imageHelp.Notes.Add("Please make sure the computer connects to a IP Network");
            imageHelp.Notes.Add("Before use \"Check Server\", make sure the Server is started");

            return imageHelp;
        }

        private static ImageContentHelpInfo GetNative(object parameter)
        {
            throw new NotImplementedException();
        }

        private static SectionHelpInfo GetCommunication(List<PhysicalPorts> collection, object parameter = null)
        {
            var info = new SectionHelpInfo("Communication");
            var imageHelp = new ImageContentHelpInfo("Select Position", IconAccessor.Help_Position1.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Computer, which connects directly to device"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Computer, which connects to IP Network"));
            imageHelp.Notes.Add("When \"Remote\" option is selected, IP network has be to available");
            info.ContentInfos.Add(imageHelp);

            foreach (var item in collection)
            {
                switch (item)
                {
                    case PhysicalPorts.Serial:          info.ContentInfos.Add(GetSerialPort());         break;
                    case PhysicalPorts.USB:             info.ContentInfos.Add(GetUsbPort());            break;
                    case PhysicalPorts.WebServer:       info.ContentInfos.Add(GetWebServer());          break;
                    case PhysicalPorts.WebClient:       info.ContentInfos.Add(GetWebClient());          break;
                    case PhysicalPorts.Native:          info.ContentInfos.Add(GetNative(parameter));    break;
                    default:                            /*Ignore*/                                      break;
                }
            }
            return info;
        }

        private static SectionHelpInfo GetGraphic()
        {
            var info = new SectionHelpInfo("Graphic");
            var imageHelp = new ImageContentHelpInfo("Graphic Components", IconAccessor.Help_Graphic1.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Number of Point will be plotted. The larger amount the more CPU is required"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Show Label of Curve on Top Right on Graphic"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Edit Axis Setting"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Edit Curve Setting"));
            info.ContentInfos.Add(imageHelp);

            imageHelp = new ImageContentHelpInfo("Axis Setting", IconAccessor.Help_Graphic2.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Label"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Unit"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Axis Line Style"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Activate Zoom function"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(5, "Activate Drag function"));
            info.ContentInfos.Add(imageHelp);

            imageHelp = new ImageContentHelpInfo("Curve Setting", IconAccessor.Help_Graphic3.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Label"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Unit"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Display Color"));
            
            info.ContentInfos.Add(imageHelp);

            return info;
        }

        private static SectionHelpInfo GetMatFile()
        {
            var info = new SectionHelpInfo("Mat File");
            var imageHelp = new ImageContentHelpInfo("Writer Setting", IconAccessor.Help_MatFile1.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Enable MatFile Writer"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Start collecting Data Sample after Start Operation is executed"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Storage folder for MatLab File"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Prefix File Name"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(5, "Amount of sample in one File"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(6, "Suffix File Name with Index, recommended to test, debug, and measure"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(7, "Collect sample only in specific duration of time"));
            info.ContentInfos.Add(imageHelp);
            return info;
        }

        private static TaskHelpInfo Generate_SoftwareGuild(object header)
        {
            TaskHelpInfo info = new TaskHelpInfo(header);

            #region Driver for PSOC4
            var psoc4 = new SectionHelpInfo("Install PSOC4 Driver");
            var bullet = new BulletTextContentHelpInfo("Important", BulletTypes.Rectangle);
            bullet.Notes.Add("Please make sure the computer has the latest Windows's update, and the internet connection is available");
            bullet.Notes.Add("Driver will be automatic downloaded and installed");
            psoc4.ContentInfos.Add(bullet);

            var imageHelp = new ImageContentHelpInfo("Connect PSOC4 to PC", IconAccessor.Help_PSOC4_Driver_1.Icon);
            psoc4.ContentInfos.Add(imageHelp);

            imageHelp = new ImageContentHelpInfo("Automatic installation PSOC4's driver", IconAccessor.Help_PSOC4_Driver_2.Icon);
            
            psoc4.ContentInfos.Add(imageHelp);           

            psoc4.ContentInfos.Add(GetDeviceManager());

            imageHelp = new ImageContentHelpInfo("Find Communication Port", IconAccessor.Help_PSOC4_Driver_3.Icon);
            imageHelp.Notes.Add("The COM# must be matched in the Connection's Port in Configuration");
            psoc4.ContentInfos.Add(imageHelp);
            #endregion

            #region Driver for PSOC5
            var psoc5 = new SectionHelpInfo("Install PSOC5 Driver");
            bullet = new BulletTextContentHelpInfo("Important", BulletTypes.Rectangle);
            bullet.Notes.Add("Please make sure the computer has the latest Windows's update, and the internet connection is available");
            bullet.Notes.Add("Driver will be automatic downloaded and installed");
            psoc5.ContentInfos.Add(bullet);

            imageHelp = new ImageContentHelpInfo("Connect PSOC5 to PC", IconAccessor.Help_PSOC5_Driver_1.Icon);
            psoc5.ContentInfos.Add(imageHelp);

            imageHelp = new ImageContentHelpInfo("Automatic installation PSOC5's driver", IconAccessor.Help_PSOC5_Driver_2.Icon);            
            psoc5.ContentInfos.Add(imageHelp);

            psoc5.ContentInfos.Add(GetDeviceManager());

            imageHelp = new ImageContentHelpInfo("Find USB", IconAccessor.Help_PSOC5_Driver_3.Icon);
            psoc5.ContentInfos.Add(imageHelp);
            #endregion

            #region config
            var config = new SectionHelpInfo("Configuration");
            imageHelp = new ImageContentHelpInfo("Support function", IconAccessor.Help_Basic1.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Support functions for Configuration"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Create new Configuration"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Load Configuration from a file"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Edit current Configuration"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(5, "Save current Configuration from further use"));
            config.ContentInfos.Add(imageHelp);

            imageHelp = new ImageContentHelpInfo("Create a new Configuration", IconAccessor.Help_Basic4.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Select Task for Software"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Help to set-up Configuration for selected Task"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Use Configuration"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Remove Configuration"));
            config.ContentInfos.Add(imageHelp);
            #endregion

            #region operation
            var operation = new SectionHelpInfo("Operation");
            imageHelp = new ImageContentHelpInfo("Support function", IconAccessor.Help_Basic2.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Support functions for Operation"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Start Software and Microcontroller operation"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Stop Software and Microcontroller operation"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Read, Edit Microcontroller Setting"));
            operation.ContentInfos.Add(imageHelp);

            imageHelp = new ImageContentHelpInfo("Edit Microcontroller Setting", IconAccessor.Help_Basic5.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "RF Communication Channel "));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Address ID to receive Package from Slave"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Address ID to transmision Package to Slave"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Write new Setting to MicroController"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(5, "Next, the device must be disconnected by remove from USB port"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(6, "Finally, plug in the device in USB port, and use normally"));
            operation.ContentInfos.Add(imageHelp);
            #endregion

            #region fileReader
            var fileReader = new SectionHelpInfo("File Reader(Basic)");
            imageHelp = new ImageContentHelpInfo("Support function", IconAccessor.Help_Basic3.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Support functions for File Reader"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Load file"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Embedded functions for specific file"));
            fileReader.ContentInfos.Add(imageHelp);
            #endregion

            info.SectionInfos.AddItems(psoc4, psoc5, config, operation, fileReader);
            return info;
        }
        #endregion

        #region View
        private static TaskHelpInfo Generate_View_Press()
        {
            var info = new TaskHelpInfo("Detect Hose Clamp Pliers closed");
            var graphicView = new SectionHelpInfo("Graphic View");
            var imageHelp = new ImageContentHelpInfo("Real-Time Parameter Graphic", IconAccessor.Help_Press2.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Label of Curve, this can be activated in Configuration => Graphic => Show Legend"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Vertical Axis in mV"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Horizontal Axis in s"));
            graphicView.ContentInfos.Add(imageHelp);

            var tableView = new SectionHelpInfo("Table View");
            imageHelp = new ImageContentHelpInfo("Closure Action Table", IconAccessor.Help_Press3.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Slave Device ID"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Detected Closure amount"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Detected Closure Time"));
            tableView.ContentInfos.Add(imageHelp);

            var controlLocal = new SectionHelpInfo("Control Panel(Local)");
            imageHelp = new ImageContentHelpInfo("Support Commands", IconAccessor.Help_Press1.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Start collecting data sample"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Start collecting data sample, and write to matlab format file"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Show Matlab Parameter Column Format"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Check if device status: Avalibility, Configuration..."));
            imageHelp.IndexInfos.Add(new TextIndexInfo(5, "Enable or disable data sample function in slave device"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(6, "Show Setting from slave device"));
            imageHelp.Notes.Add("The function should be used if there is only one device available.");
            imageHelp.Notes.Add("Due to switching Transmision/Receive of RF, there may be lost package if Debug is available");
            controlLocal.ContentInfos.Add(imageHelp);

            imageHelp = new ImageContentHelpInfo("Edit Device Setting", IconAccessor.Help_Press9.Icon);
            imageHelp.Notes.Add("Please read the attached Documentation, in order to edit setting correctly");
            controlLocal.ContentInfos.Add(imageHelp);

            var controlRemote = new SectionHelpInfo("Control Panel(Remote)");
            imageHelp = new ImageContentHelpInfo("Support Commands", IconAccessor.Help_Press4.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Show available Closure History File in Server"));
            controlRemote.ContentInfos.Add(imageHelp);

            imageHelp = new ImageContentHelpInfo("Available File in Server", IconAccessor.Help_Press5.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "File Name, in Format of Day_Month_Year"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "File Size"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Download, and open File"));
            controlRemote.ContentInfos.Add(imageHelp);

            var fileReader = new SectionHelpInfo("File Reader(Specific)");
            imageHelp = new ImageContentHelpInfo("File Content, and Support Commands", IconAccessor.Help_Press6.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "File Reader view"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Save requested File in Local Computer"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Filter File Content"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "File Name, in format of Day_Month_Year"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(5, "Time of closure action"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(6, "Amount of closure action"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(7, "Slave Device ID"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(8, "Start and End Time of Device in History File"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(9, "Total Amount of closure action in History File"));
            fileReader.ContentInfos.Add(imageHelp);

            imageHelp = new ImageContentHelpInfo("Filter Parameter", IconAccessor.Help_Press7.Icon);
            imageHelp.IndexInfos.Add(new TextIndexInfo(1, "Find Slave Device ID"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(2, "Requested ID"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(3, "Find in Time"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(4, "Start Time"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(5, "Duration of Time"));
            imageHelp.IndexInfos.Add(new TextIndexInfo(6, "End Time"));
            fileReader.ContentInfos.Add(imageHelp);

            imageHelp = new ImageContentHelpInfo("Filtered Content", IconAccessor.Help_Press8.Icon);
            fileReader.ContentInfos.Add(imageHelp);

            info.SectionInfos.AddItems(graphicView, tableView, controlLocal, controlRemote, fileReader);
            return info;
        }

        private static TaskHelpInfo Generate_View_Capacitor()
        {
            var info = new TaskHelpInfo(string.Empty);

            return info;
        }

        private static TaskHelpInfo Generate_View_Acceleration()
        {
            var info = new TaskHelpInfo(string.Empty);

            return info;
        }

        private static TaskHelpInfo Generate_View_uBalance()
        {
            var info = new TaskHelpInfo(string.Empty);

            return info;
        }

        private static TaskHelpInfo Generate_View_RfTrans()
        {
            var info = new TaskHelpInfo(string.Empty);

            return info;
        }

        private static TaskHelpInfo Generate_View_MotorCtr()
        {
            var info = new TaskHelpInfo(string.Empty);

            return info;
        }
        #endregion

        #region Config
        private static TaskHelpInfo Generate_Config_Press()
        {
            var connection = new List<PhysicalPorts>() { PhysicalPorts.Serial, PhysicalPorts.USB, PhysicalPorts.WebServer, PhysicalPorts.WebClient };
            var info = new TaskHelpInfo("Detect Hose Clamp Pliers closed");
            info.SectionInfos.Add(GetCommunication(connection));
            info.SectionInfos.Add(GetGraphic());
            info.SectionInfos.Add(GetMatFile());
            return info;
        }

        private static TaskHelpInfo Generate_Config_Capacitor()
        {
            var connection = new List<PhysicalPorts>() { PhysicalPorts.Serial, PhysicalPorts.USB };
            var info = new TaskHelpInfo("Measure Capacitors");
            info.SectionInfos.Add(GetCommunication(connection));
            return info;
        }

        private static TaskHelpInfo Generate_Config_Acceleration()
        {
            var connection = new List<PhysicalPorts>() { PhysicalPorts.Serial, PhysicalPorts.USB };
            var info = new TaskHelpInfo("Measure Acceleration");
            info.SectionInfos.Add(GetCommunication(connection));
            return info;
        }

        private static TaskHelpInfo Generate_Config_uBalance()
        {
            var connection = new List<PhysicalPorts>() { PhysicalPorts.Serial, PhysicalPorts.USB };
            var info = new TaskHelpInfo("uBalance System");
            info.SectionInfos.Add(GetCommunication(connection));
            return info;
        }

        private static TaskHelpInfo Generate_Config_RfTrans()
        {
            var connection = new List<PhysicalPorts>() { PhysicalPorts.Serial, PhysicalPorts.USB };
            var info = new TaskHelpInfo("Test RF Transceiver");
            info.SectionInfos.Add(GetCommunication(connection));
            return info;
        }

        private static TaskHelpInfo Generate_Config_MotorCtr()
        {
            var connection = new List<PhysicalPorts>() { PhysicalPorts.Serial, PhysicalPorts.USB };
            var info = new TaskHelpInfo("Motor Controller System");
            info.SectionInfos.Add(GetCommunication(connection));
            return info;
        }
        #endregion
    }
}
