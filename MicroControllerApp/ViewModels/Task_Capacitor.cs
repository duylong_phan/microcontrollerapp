﻿using LongModel.ViewModels;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.ViewClasses.Graphics;
using MicroControllerApp.ViewModels.Helpers;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.ViewModels
{
    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        #region Local
        private void Cap_InitializeSW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as CapSetting;
            Setting_InitializeSW(setting);
            this.currentTime = 0;            
        }

        private void Cap_InitializeHW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as CapSetting;
            Setting_InitializeHW(setting);
        }

        private void Cap_Update()
        {
            if (this.packageQueue.Count == 0)
            {
                return;
            }

            #region Get Collection
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as CapSetting;
            var sampleCollection = new List<CapSamplePackage>();
            DataPackage package = null;
            while (this.packageQueue.Count > 0)
            {
                this.packageQueue.TryDequeue(out package);
                if (package is ISharedPackage)
                {
                    Setting_Update(setting, package as ISharedPackage);
                }
                else if(package is CapSamplePackage)
                {
                    var capSamplePackage = package as CapSamplePackage;                    
                    for (int i = 0; i < CapSamplePackage.CapAmount; i++)
                    {
                        capSamplePackage.Capacities[i] = capSamplePackage.Durations[i] / (double)setting.ChargeResistor;
                    }
                    sampleCollection.Add(capSamplePackage);
                }
            }
            #endregion

            #region Update View
            //Take the last Sample
            if (package is CapSamplePackage)
            {
                var capSamplePackage = package as CapSamplePackage;
                this.OutputParameter.Capacitor1 = capSamplePackage.Capacities[0];
                this.OutputParameter.Capacitor2 = capSamplePackage.Capacities[1];
                this.OutputParameter.Capacitor3 = capSamplePackage.Capacities[2];
                this.OutputParameter.Capacitor4 = capSamplePackage.Capacities[3];
            }

            //Add Sample into graphic
            if (sampleCollection.Count > 0)
            {
                var pointCollection = new List<DataPoint>[CapSamplePackage.CapAmount];
                var curves = new DataCurve[CapSamplePackage.CapAmount];
                for (int i = 0; i < CapSamplePackage.CapAmount; i++)
                {
                    pointCollection[i] = new List<DataPoint>();
                    curves[i] = this.OutputParameter.PlotModel.Series[i] as DataCurve;
                }

                //go throung each sample
                foreach (var item in sampleCollection)
                {
                    //calculate Capacitor, and initialize a Data Point for each curve
                    for (int i = 0; i < CapSamplePackage.CapAmount; i++)
                    {
                        pointCollection[i].Add(new DataPoint(this.currentTime, item.Capacities[i]));
                    }
                    this.currentTime += this.samplePeriod;
                }                

                //Check overflow
                int remain = setting.GraphicSetting.PointAmount - curves[0].Points.Count;
                int overflow = pointCollection[0].Count - remain;                
                if (overflow > 0)
                {
                    for (int i = 0; i < CapSamplePackage.CapAmount; i++)
                    {
                        curves[i].Points.RemoveRange(0, overflow);
                    }
                }
                //add Point
                for (int i = 0; i < CapSamplePackage.CapAmount; i++)
                {
                    curves[i].Points.AddRange(pointCollection[i]);
                }

                //View is available
                if (this.OutputParameter.HasView(AppViews.Graphic))
                {
                    this.OutputParameter.UpdateGraphic();
                }
            }
            #endregion

            #region Mat
            if (this.matFileHandler != null && this.matFileHandler.IsEnabled)
            {
                foreach (var item in sampleCollection)
                {
                    var container = new double[CapSamplePackage.CapAmount * 2 + 1];
                    container[0] = (DateTime.Now.Ticks - this.startTick) / (double)TimeSpan.TicksPerMillisecond;
                    Array.Copy(item.Durations, 0, container, 1, CapSamplePackage.CapAmount);
                    Array.Copy(item.Capacities, 0, container, CapSamplePackage.CapAmount + 1, CapSamplePackage.CapAmount);

                    this.matFileHandler.Add(container);
                }
                this.OutputParameter.MatSampleAmount = this.matFileHandler.SampleAmount;
            }
            #endregion
        }

        private void Cap_DisposeSW()
        {
            Setting_DisposeSW(this.InputParameter.CurrentConfig.CurrentSetting as SettingBase);
        }

        private void Cap_DisposeHW()
        {
            Setting_DisposeHW(this.InputParameter.CurrentConfig.CurrentSetting as SettingBase);
        }

        #endregion

        #region Remote
        private void Cap_OnRequested(HttpListenerRequest request, HttpListenerResponse respone)
        {
            throw new NotImplementedException();
        }

        private void Cap_OnResponsed(string arg1, byte[] arg2)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
