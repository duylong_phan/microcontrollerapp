﻿using LongInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Interfaces
{
    public interface IPlot : ICopyable<IPlot>
    {
        bool IsLegendVisible { get; set; }
    }
}
