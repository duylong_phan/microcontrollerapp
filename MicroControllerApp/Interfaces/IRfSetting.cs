﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Interfaces
{
    public interface IRfSetting
    {
        byte RfChannel { get; set; }
        string RfRxAddress { get; set; }
        string RfTxAddress { get; set; }
    }
}
