﻿using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.Models.IO.Files;
using System.Xml.Serialization;
using OxyPlot;
using OxyPlot.Axes;
using System.Windows.Media;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using LongInterface.Models.IO.Files;

namespace MicroControllerApp.Models.DataClasses.Settings
{
    public class MotorCtrSetting : SettingBase, IGraphic, IMatFile, INotifyFileChanged<FileChangedSetting>
    {
        #region Getter, Setter
        [XmlElement("GraphicSetting")]
        public GraphicSetting GraphicSetting { get; set; }
        [XmlElement("MatFileSetting")]
        public FileSingleSetting MatFileSetting { get; set; }
        [XmlElement("DataOption")]
        public MotorCtrDataOption DataOption { get; set; }
        [XmlElement("FileChanged")]
        public FileChangedSetting FileChangedSetting { get; set; }
        [XmlElement("UnbalanceSetting")]
        public MotorUnbalanceSetting UnbalanceSetting { get; set; }
        [XmlIgnore]
        public SampleInfo SampleInfo { get; set; }
        #endregion

        #region Field
        private Action<object> onFileChanged;
        #endregion

        #region Constructor
        public MotorCtrSetting()
            : base(MicroTasks.MotorCtr, new RemoteSetting(), new ServerSetting(), ErrorNoticicationTypes.AppStatus)
        {
            this.MatFileSetting = new FileSingleSetting(App.DataPath, "Motors_Data", ".mat", 10000, 1, true, 60, false, true, false);
            this.DataOption = new MotorCtrDataOption();
            this.FileChangedSetting = new FileChangedSetting(false, true, string.Empty, FileChangedResponses.NewLine, "Info", 10);
            this.UnbalanceSetting = new MotorUnbalanceSetting();

            #region Graphic
            this.GraphicSetting = new GraphicSetting();
            this.GraphicSetting.AxisCollection.Add(new AxisInfo("Sample Index", string.Empty, false, false, LineStyle.LongDash, AxisPosition.Bottom));
            this.GraphicSetting.AxisCollection.Add(new AxisInfo("Amplitude", string.Empty, false, false, LineStyle.LongDash, AxisPosition.Left));
            this.GraphicSetting.CurveCollection.Add(new CurveInfo("Battery[V]", Colors.Blue, LineStyle.Solid));
            this.GraphicSetting.CurveCollection.Add(new CurveInfo("Out Source[V]", Colors.Red, LineStyle.Solid));
            this.GraphicSetting.CurveCollection.Add(new CurveInfo("PWM Freq[kHz]", Colors.Green, LineStyle.Solid));
            #endregion

            #region SampleInfo
            var columns = new List<ColumnInfo>();
            columns.Add(new ColumnInfo("Time", 1, "Sample Time", "s"));
            columns.Add(new ColumnInfo("Battery", 2, "Battery Voltage", "V"));
            columns.Add(new ColumnInfo("Out Source", 2, "Volage Source for Output Signal", "V"));
            columns.Add(new ColumnInfo("PWM Freq", 3, "PWM Source Signal Frequency", "kHz"));
            this.SampleInfo = new SampleInfo(-1, "User definition", columns);
            #endregion
        }
        #endregion

        #region Public Method
        public override DataPackage ParseBuffer(byte[] buffer)
        {
            DataPackage package = null;
            switch (buffer[0])
            {
                case (byte)PcCommands.MotorCtr_Status:      package = new MotorCtrStatusPackage(buffer[0], buffer);     break;                
                case (byte)PcCommands.MotorCtr_DataSample:  package = new MotorCtrSamplePackage(buffer[0], buffer);     break;
                default:                                    /*Ignore*/                                                  break;
            }
            return package;
        }

        public void SetFileChangedAction(Action<object> action)
        {
            this.onFileChanged = action;
        }

        public void OnFileChanged(object parameter)
        {
            if (this.onFileChanged != null)
            {
                this.onFileChanged(parameter);
            }
        }
        #endregion
    }
}
