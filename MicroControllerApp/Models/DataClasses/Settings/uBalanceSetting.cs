﻿using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Settings
{
    public class uBalanceSetting : SettingBase
    {
        #region Const, Static
        public const string ActionExtension = ".uBa";
        public static string ActionFilter {get; private set;}

        static uBalanceSetting()
        {
            ActionFilter = string.Format("uBalance system Action file (*{0})|*{0}", ActionExtension);
        }
        #endregion

        #region Constructor
        public uBalanceSetting()
            : base(MicroTasks.uBalance, 
                   new RemoteSetting(), 
                   new ServerSetting())
        {

        }
        #endregion

        #region Public Method
        public override DataPackage ParseBuffer(byte[] buffer)
        {
            DataPackage package = null;

            switch (buffer[0])
            {
                case (byte)PcCommands.uBalance_Status:
                    package = new uBalanceStatusPackage(buffer[0], buffer);
                    break;

                default:
                    break;
            }

            return package;
        }
        #endregion
    }
}
