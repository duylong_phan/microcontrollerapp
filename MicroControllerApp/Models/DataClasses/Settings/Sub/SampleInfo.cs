﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Settings.Sub
{
    public class SampleInfo
    {
        #region Getter
        public double Frequecy { get; private set; }
        public string Note { get; private set; }
        public List<ColumnInfo> Columns { get; private set; }
        #endregion

        #region Constructor
        public SampleInfo()
            : this(-1, string.Empty, null)
        {

        }

        public SampleInfo(double frequency, string note, List<ColumnInfo> columns)
        {
            this.Frequecy = frequency;
            this.Note = note;
            this.Columns = columns;
        }
        #endregion
    }
}
