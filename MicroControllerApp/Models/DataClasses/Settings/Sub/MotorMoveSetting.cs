﻿using LongModel.BaseClasses;
using MicroControllerApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Settings.Sub
{
    public class MotorMoveSetting : ObservableObject, IMoveMotor
    {
        #region Getter
        public byte Type { get; private set; }
        #endregion

        #region Getter, Setter
        private bool isForward;
        private ushort pulse;

        public bool IsForward
        {
            get { return this.isForward; }
            set
            {
                this.SetProperty(ref this.isForward, value);
            }
        }        
        public ushort Pulse
        {
            get { return this.pulse; }
            set
            {
                this.SetProperty(ref this.pulse, value);
            }
        }

        public object PulseRule { get; set; }
        #endregion

        #region Constructor
        public MotorMoveSetting(byte type, bool isForward, ushort pulse)
        {
            this.Type = type;
            this.isForward = isForward;
            this.pulse = pulse;
        }
        #endregion
    }
}
