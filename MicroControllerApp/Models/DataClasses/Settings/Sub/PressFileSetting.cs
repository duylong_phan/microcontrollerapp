﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.BaseClasses;
using LongInterface.Models;
using LongInterface.Models.IO.Files;
using LongModel.Helpers;

namespace MicroControllerApp.Models.DataClasses.Settings.Sub
{
    public class PressFileSetting : FileSettingBase, ICopyable<IFileSettingBase>
    {
        #region Getter, Setter
        private int pointAmount;
        public int PointAmount
        {
            get { return this.pointAmount; }
            set
            {
                this.SetProperty(ref this.pointAmount, value);
            }
        }
        #endregion

        #region Constructor
        public PressFileSetting()
            : base(string.Empty, 0, false, false,false)
        {

        }

        public PressFileSetting(string directory, int duration, bool useDuration, bool isEnabled, bool captureOnStart)
            : base(directory, duration, useDuration, isEnabled, captureOnStart)
        {
            this.pointAmount = 100;
        }
        #endregion

        #region Public Method
        public override object CloneModel()
        {
            var setting = new PressFileSetting();
            setting.CopyProperties(this);
            return setting;
        }

        public void CopyProperties(IFileSettingBase item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }
        #endregion
    }
}
