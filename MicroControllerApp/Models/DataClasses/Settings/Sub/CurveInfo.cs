﻿using LongModel.BaseClasses;
using LongModel.Helpers;
using MicroControllerApp.Interfaces;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings.Sub
{
    public class CurveInfo : ObservableObject, ICurveInfo
    {
        #region Getter, setter
        private string title;
        private LineStyle lineStyle;
        private Color colorUI;

        [XmlAttribute("Title")]
        public string Title
        {
            get { return this.title; }
            set
            {
                this.SetProperty(ref this.title, value);
            }
        }
        [XmlAttribute("LineStyle")]
        public LineStyle LineStyle
        {
            get { return this.lineStyle; }
            set
            {
                this.SetProperty(ref this.lineStyle, value);
            }
        }
        [XmlAttribute("Color")]
        public string ColorXML
        {
            get
            {
                return string.Format("{0} {1} {2}", this.colorUI.R, this.colorUI.G, this.colorUI.B);
            }
            set
            {
                try
                {
                    string[] texts = value.Split(' ');
                    this.ColorUI = System.Windows.Media.Color.FromRgb(byte.Parse(texts[0]), byte.Parse(texts[1]), byte.Parse(texts[2]));
                }
                catch (Exception)
                {
                    this.ColorUI = Colors.Black;
                }
            }
        }


        [XmlIgnore]
        public Color ColorUI
        {
            get
            {
                return this.colorUI;
            }
            set
            {
                this.SetProperty(ref this.colorUI, value);
            }
        }
        [XmlIgnore]
        public OxyColor Color
        {
            get
            {
                return OxyColor.FromRgb(this.colorUI.R, this.colorUI.G, this.colorUI.B);
            }
            set
            {
                this.ColorUI = System.Windows.Media.Color.FromRgb(value.R, value.G, value.B);
            }
        }
        [XmlIgnore]
        public ICommand OpenSetting { get; set; }
        [XmlIgnore]
        public object LineStyleCollection { get; set; }
        #endregion

        #region Constructor
        public CurveInfo()
            : this("No title", Colors.Black, LineStyle.Solid)
        {

        }

        public CurveInfo(string title, Color color, LineStyle lineStyle)
        {
            this.title = title;
            this.colorUI = color;
            this.LineStyle = lineStyle;
        }
        #endregion

        #region Public Method
        public void CopyProperties(ICurveInfo item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }

        public object CloneModel()
        {
            CurveInfo info = new CurveInfo();
            info.CopyProperties(this);
            return info;
        }
        #endregion
    }
}
