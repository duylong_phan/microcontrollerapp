﻿using LongModel.Models.IO.Files;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings
{
    public class AccSetting : SettingBase, IMatFile, IGraphic
    {
        #region Getter, setter         
        [XmlElement("MatFileSetting")]
        public FileSingleSetting MatFileSetting { get; set; }
        [XmlElement("GraphicSetting")]
        public GraphicSetting GraphicSetting { get; set; }

        [XmlIgnore]
        public SampleInfo SampleInfo { get; set; }
        #endregion

        #region Constructor
        public AccSetting()
            : base(MicroTasks.Acceleration, new RemoteSetting(), new ServerSetting())
        {
            this.MatFileSetting = new FileSingleSetting(App.DataPath, "Acc_Data", ".mat", 10000, 1, true, 60, false, true, false);

            #region Graphic
            this.GraphicSetting = new GraphicSetting();
            this.GraphicSetting.AxisCollection.Add(new AxisInfo("Capacitance", "uF", false, false, OxyPlot.LineStyle.Dash, OxyPlot.Axes.AxisPosition.Left));
            this.GraphicSetting.AxisCollection.Add(new AxisInfo("Sample", "Index", false, false, OxyPlot.LineStyle.Dash, OxyPlot.Axes.AxisPosition.Bottom));
            for (int i = 0; i < AccSamplePackage.AccAmount; i++)
            {
                var index = i + 1;
                this.GraphicSetting.CurveCollection.Add(new CurveInfo("X" + index, Colors.Red, OxyPlot.LineStyle.Solid));
                this.GraphicSetting.CurveCollection.Add(new CurveInfo("Y" + index, Colors.Blue, OxyPlot.LineStyle.Solid));
                this.GraphicSetting.CurveCollection.Add(new CurveInfo("Z" + index, Colors.Green, OxyPlot.LineStyle.Solid));
            }
            #endregion

            #region SampleInfo
            var columns = new List<ColumnInfo>();
            columns.Add(new ColumnInfo("Time", 1, "The moment when the sample is received", "ms"));
            columns.Add(new ColumnInfo("X1", 2, "Sensor 1 X Acceleration ", "mg"));
            columns.Add(new ColumnInfo("Y1", 3, "Sensor 1 Y Acceleration", "mg"));
            columns.Add(new ColumnInfo("Z1", 4, "Sensor 1 Z Acceleration", "mg"));
            columns.Add(new ColumnInfo("X2", 5, "Sensor 2 X Acceleration ", "mg"));
            columns.Add(new ColumnInfo("Y2", 6, "Sensor 2 Y Acceleration", "mg"));
            columns.Add(new ColumnInfo("Z2", 7, "Sensor 2 Z Acceleration", "mg"));
            columns.Add(new ColumnInfo("X3", 8, "Sensor 3 X Acceleration ", "mg"));
            columns.Add(new ColumnInfo("Y3", 9, "Sensor 3 Y Acceleration", "mg"));
            columns.Add(new ColumnInfo("Z3", 10, "Sensor 3 Z Acceleration", "mg"));
            columns.Add(new ColumnInfo("X4", 11, "Sensor 4 X Acceleration ", "mg"));
            columns.Add(new ColumnInfo("Y4", 12, "Sensor 4 Y Acceleration", "mg"));
            columns.Add(new ColumnInfo("Z5", 13, "Sensor 4 Z Acceleration", "mg"));
            this.SampleInfo = new SampleInfo(-1, string.Empty, columns);
            #endregion
        }
        #endregion

        #region Public Method
        public override DataPackage ParseBuffer(byte[] buffer)
        {
            DataPackage package = null;

            switch (buffer[0])
            {
                case (byte)PcCommands.Accelerometer_Done:
                    package = new AccSamplePackage(buffer[0], buffer);
                    break;

                default:
                    //Do nothing
                    break;
            }

            return package;
        }
        #endregion
    }
}
