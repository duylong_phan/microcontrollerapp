﻿using LongModel.Models.IO.Files;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings
{
    public class CapSetting : SettingBase, IMatFile, IGraphic
    {
        #region Getter, setter
        private int chargeResistor;

        [XmlAttribute("ChargeResistor")]
        public int ChargeResistor
        {
            get { return this.chargeResistor; }
            set
            {
                this.SetProperty(ref this.chargeResistor, value);
            }
        }
        [XmlElement("MatFileSetting")]
        public FileSingleSetting MatFileSetting { get; set; }
        [XmlElement("GraphicSetting")]
        public GraphicSetting GraphicSetting { get; set; }
        

        [XmlIgnore]
        public SampleInfo SampleInfo { get; set; }
        [XmlIgnore]
        public object ChargeResistorRule { get; set; }
        #endregion

        #region Constructor
        public CapSetting()
            : base(MicroTasks.Capacitor, new RemoteSetting(), new ServerSetting())
        {
            this.chargeResistor = 10000;
            this.MatFileSetting = new FileSingleSetting(App.DataPath, "Cap_Data", ".mat", 10000, 1, true, 60, false, true, false);

            #region Graphic
            this.GraphicSetting = new GraphicSetting();
            this.GraphicSetting.AxisCollection.Add(new AxisInfo("Capacitance", "uF", false, false, OxyPlot.LineStyle.Dash, OxyPlot.Axes.AxisPosition.Left));
            this.GraphicSetting.AxisCollection.Add(new AxisInfo("Sample", "Index", false, false, OxyPlot.LineStyle.Dash, OxyPlot.Axes.AxisPosition.Bottom));
            this.GraphicSetting.CurveCollection.Add(new CurveInfo("C1", Colors.Red, OxyPlot.LineStyle.Solid));
            this.GraphicSetting.CurveCollection.Add(new CurveInfo("C2", Colors.Blue, OxyPlot.LineStyle.Solid));
            this.GraphicSetting.CurveCollection.Add(new CurveInfo("C3", Colors.Green, OxyPlot.LineStyle.Solid));
            this.GraphicSetting.CurveCollection.Add(new CurveInfo("C4", Colors.Gray, OxyPlot.LineStyle.Solid));
            #endregion

            #region SampleInfo
            var columns = new List<ColumnInfo>();
            columns.Add(new ColumnInfo("Time", 1, "The moment when the sample is received", "ms"));
            columns.Add(new ColumnInfo("Duration 1", 2, "Charging duration of C1", "us"));
            columns.Add(new ColumnInfo("Duration 2", 3, "Charging duration of C2", "us"));
            columns.Add(new ColumnInfo("Duration 3", 4, "Charging duration of C3", "us"));
            columns.Add(new ColumnInfo("Duration 4", 5, "Charging duration of C4", "us"));
            columns.Add(new ColumnInfo("Capacitance 1", 6, "C1 respected to given charged resistor", "uF"));
            columns.Add(new ColumnInfo("Capacitance 2", 7, "C2 respected to given charged resistor", "uF"));
            columns.Add(new ColumnInfo("Capacitance 3", 8, "C3 respected to given charged resistor", "uF"));
            columns.Add(new ColumnInfo("Capacitance 4", 9, "C4 respected to given charged resistor", "uF"));
            this.SampleInfo = new SampleInfo(-1, string.Empty, columns);
            #endregion
        }
        #endregion

        #region Public Method
        public override DataPackage ParseBuffer(byte[] buffer)
        {
            DataPackage package = null;

            switch (buffer[0])
            {
                case (byte)PcCommands.Cap_Done:
                    package = new CapSamplePackage(buffer[0], buffer);
                    break;

                default:
                    //Do nothing
                    break;
            }

            return package;
        }
        #endregion
    }
}
