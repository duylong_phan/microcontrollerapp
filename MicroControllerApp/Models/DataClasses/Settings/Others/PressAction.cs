﻿using LongInterface.Models;
using MicroControllerApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    public enum PressTypes
    {
        /// <summary>Get Device Status</summary>
        DeviceStatus = 0x01,
        /// <summary>Press Action is detected</summary>
        Pressed = 0x02,
        /// <summary>Parameter Curve is received</summary>
        Debug = 0x04,
        /// <summary>Request Setting from Slave Device</summary>
        ReadSetting = 0x08,
        /// <summary>Write new Setting to Slave Device</summary>
        WriteSetting = 0x10        
    }

    public class PressAction : IBuffer
    {
        #region Static
        public static PressAction Ping
        {
            get
            {
                return new PressAction((byte)PressTypes.DeviceStatus);
            }
        }

        public static PressAction ToggleDebug
        {
            get
            {
                return new PressAction((byte)PressTypes.Debug);
            }
        }
        #endregion

        #region Gettter
        public byte Type { get; private set; }
        #endregion

        #region Constructor
        public PressAction(byte type)
        {
            this.Type = type;
        }
        #endregion

        #region Public method
        public virtual  byte[] GetBytes()
        {
            return new byte[0];
        }
        #endregion
    }
}
