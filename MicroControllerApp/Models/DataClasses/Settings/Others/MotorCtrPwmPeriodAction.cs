﻿using LongInterface.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    public class MotorCtrPwmPeriodAction : IBuffer
    {
        #region Getter
        public ushort Period { get; private set; }
        #endregion

        #region Constructor
        public MotorCtrPwmPeriodAction(ushort period)
        {
            this.Period = period;
        }
        #endregion

        #region Public Method
        public byte[] GetBytes()
        {
            byte[] data = null;
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.Period);
                data = stream.ToArray();
            }
            return data;
        }
        #endregion
    }
}
