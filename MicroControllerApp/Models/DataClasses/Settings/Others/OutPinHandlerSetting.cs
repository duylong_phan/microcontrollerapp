﻿using LongInterface.Models;
using LongModel.BaseClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MicroControllerApp.Models.ViewClasses.Infos;
using System.Collections.ObjectModel;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    public enum OutPinCommands
    {
        Non = 0,
        Reset = 1,
        Set = 2,
        Toggle = 3,
        PosTrigger = 4,
        NegTrigger = 5,
        Rect = 6,
        PWM = 7,
        PWM_Auto = 8,
        PWM_Servo = 9,
        PWM_Servo_Auto = 10,
        PWM_Servo_SV1270TG = 11,
        PWM_Servo_SV1270TG_Auto = 12
    }

    public class OutPinHandlerSetting : ObservableObject, IGetText, IBuffer, IFilePath
    {
        #region Const
        public const int ServorOriginal = 100;
        public const int MaxPercent = 100;
        public const int MaxDegree_SV1270TG = 85;
        #endregion

        #region Getter        
        public string Text { get; private set; }
        public byte ID { get; private set; }
        public ObservableCollection<DoubleMatVariableInfo> InputVariables { get; private set; }
        public int ServoPercentView
        {
            get
            {
                return this.servoPercent - ServorOriginal;
            }
        }
        public double ServoDegreeView
        {
            get
            {
                double degree = 0;

                switch (this.command)
                {
                    case OutPinCommands.PWM_Servo_SV1270TG:
                        degree = ((this.servoPercent - 100) / 100.0) * MaxDegree_SV1270TG;
                        break;

                    default:
                        break;
                }
                return degree;
            }
        }
        #endregion

        #region Getter, setter
        private OutPinCommands command;
        private ushort duration;
        private ushort pwmPercent;
        private ushort servoPercent;
        private bool isFuncEnabled;
        private int autoProgressPercent;
        private DoubleMatVariableInfo selectedVariable;
        
        public OutPinCommands Command
        {
            get { return this.command; }
            set
            {
                this.SetProperty(ref this.command, value);
            }
        }
        public ushort Duration
        {
            get { return this.duration; }
            set
            {
                this.SetProperty(ref this.duration, value);
            }
        }
        public ushort PwmPercent
        {
            get { return this.pwmPercent; }
            set
            {
                this.SetProperty(ref this.pwmPercent, value);
            }
        }        
        public ushort ServoPercent
        {
            get { return this.servoPercent; }
            set
            {
                var isChanged = this.SetProperty(ref this.servoPercent, value);
                if (isChanged)
                {
                    OnPropertyChanged("ServoPercentView");
                    OnPropertyChanged("ServoDegreeView");
                }
            }
        }
        public bool IsFuncEnabled
        {
            get { return this.isFuncEnabled; }
            set
            {
                this.SetProperty(ref this.isFuncEnabled, value);
            }
        }
        public int AutoProgressPercent
        {
            get { return this.autoProgressPercent; }
            set
            {
                this.SetProperty(ref this.autoProgressPercent, value);
            }
        }
        public DoubleMatVariableInfo SelectedVariable
        {
            get { return this.selectedVariable; }
            set
            {
                this.SetProperty(ref this.selectedVariable, value);
            }
        }

        public object CommandCollection { get; set; }
        public object DurationRule { get; set; }
        public object ResetPin { get; set; }
        public object SetPin { get; set; }
        public object ExeCommand { get; set; }
        public object LoadFile { get; set; }

        public int ContentIndex { get; set; }
        public string FilePath { get; set; }
        public object Tag { get; set; }
        #endregion

        #region Constructor
        public OutPinHandlerSetting(string text, byte id)
        {
            this.IsFuncEnabled = false;
            this.Text = text;
            this.ID = id;
            this.command = OutPinCommands.Non;
            this.duration = 100;
            this.pwmPercent = 0;
            this.ServoPercent = ServorOriginal;

            this.ContentIndex = 0;
            this.autoProgressPercent = 0;
            this.selectedVariable = null;
            this.InputVariables = new ObservableCollection<DoubleMatVariableInfo>();

            this.FilePath = string.Empty;
            this.Tag = "Matlab File (*.mat)|*.mat";
        }
        #endregion

        #region Public Method
        
        public byte[] GetBytes()
        {
            return GetBytes(this.command);
        }
               
        public byte[] GetBytes(OutPinCommands command)
        {
            byte[] data = null;

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.ID);
                writer.Write((byte)command);
                switch (command)
                {
                    case OutPinCommands.PosTrigger:
                    case OutPinCommands.NegTrigger:
                    case OutPinCommands.Rect:                       writer.Write(this.duration);        break;
                    case OutPinCommands.PWM:                        
                    case OutPinCommands.PWM_Auto:                   writer.Write(this.pwmPercent);      break;
                    case OutPinCommands.PWM_Servo:                        
                    case OutPinCommands.PWM_Servo_Auto:
                    case OutPinCommands.PWM_Servo_SV1270TG:         
                    case OutPinCommands.PWM_Servo_SV1270TG_Auto:    writer.Write(this.servoPercent);    break;
                    default:                                        /*ignore*/                          break;
                }                         
                data = stream.ToArray();
            }
            return data;
        }
        #endregion
    }
}
