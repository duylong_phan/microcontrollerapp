﻿using LongInterface.Models;
using LongModel.BaseClasses;
using MicroControllerApp.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    [Flags]
    public enum MotorCtrSampleTypes
    {
        Battery = 1 << 0,
        Pulse = 1 << 1,
        PwmFreq = 1 << 2,
    }

    public class MotorCtrDataOption : ObservableObject, IBuffer
    {
        #region Const
        public const int OptionAmount = 3;
        #endregion

        #region Getter, setter
        private bool hasBattery;
        private bool hasPulse;
        private bool hasPwmFrequency;
        private byte frequency;

        [XmlAttribute("HasBattery")]
        public bool HasBattery
        {
            get { return this.hasBattery; }
            set
            {
                this.SetProperty(ref this.hasBattery, value);
            }
        }
        [XmlAttribute("HasPulse")]
        public bool HasPulse
        {
            get { return this.hasPulse; }
            set
            {
                this.SetProperty(ref this.hasPulse, value);
            }
        }
        [XmlAttribute("HasPwmFrequency")]
        public bool HasPwmFrequency
        {
            get { return this.hasPwmFrequency; }
            set
            {
                this.SetProperty(ref this.hasPwmFrequency, value);
            }
        }
        [XmlAttribute("Frequency")]
        public byte Frequency
        {
            get { return this.frequency; }
            set
            {
                this.SetProperty(ref this.frequency, value);
            }
        }

        [XmlIgnore]
        public object FrequencyRule { get; set; }
        #endregion

        #region Constructor
        public MotorCtrDataOption()
        {
            this.hasBattery = true;
            this.hasPulse = true;
            this.hasPwmFrequency = true;
            this.frequency = 10;
        }
        #endregion

        #region Public Method
        public byte[] GetBytes()
        {
            uint flag = 0;
            if (this.hasBattery)        flag |= (uint)MotorCtrSampleTypes.Battery;
            if (this.hasPulse)          flag |= (uint)MotorCtrSampleTypes.Pulse;
            if (this.hasPwmFrequency)   flag |= (uint)MotorCtrSampleTypes.PwmFreq;

            byte[] data = null;
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))           
            {
                writer.Write(flag);
                writer.Write(this.frequency);
                data = stream.ToArray();
            }

            return data;
        }
        #endregion
    }
}
