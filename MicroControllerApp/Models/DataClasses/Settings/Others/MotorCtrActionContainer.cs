﻿using LongInterface.ViewModels.Logics;
using LongModel.Collections;
using MicroControllerApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    public class MotorCtrActionContainer : ManualObservableCollection<MotorCtrMotorAction>, IDataEditor<MotorControl1Flags>
    {
        #region Getter, setter
        public ICommand AddAction { get; set; }
        public ICommand DownAction { get; set; }
        public ICommand EditAction { get; set; }
        public ICommand RemoveAction { get; set; }
        public ICommand UpAction { get; set; }
        #endregion

        #region Getter
        public string Text { get; private set; }
        public MotorControl1Flags Type { get; private set; }
        #endregion

        #region Constructor
        public MotorCtrActionContainer(string text, MotorControl1Flags type)
        {
            this.Text = text;
            this.Type = type;
        }
        #endregion
    }
}
