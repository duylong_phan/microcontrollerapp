﻿using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class SubSlaveSettingPackage : DataPackage, ISharedPackage
    {
        #region Const
        public const int PosIndex = 2;
        public const int SettingIndex = 3;
        public const int SettingSize = 29;
        #endregion

        #region Getter
        public int Position { get; private set; }
        #endregion
        
        #region Constructor
        public SubSlaveSettingPackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            this.Position = buffer[PosIndex];
        }
        #endregion
    }
}
