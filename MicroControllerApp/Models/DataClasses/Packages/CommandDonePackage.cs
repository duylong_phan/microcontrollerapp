﻿using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class CommandDonePackage : DataPackage, ISharedPackage
    {
        #region Const
        public const int CommandIndex = 2;
        #endregion

        #region Getter
        public PcCommands Command { get; private set; }
        #endregion

        #region Constructor
        public CommandDonePackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            this.Command = (PcCommands)buffer[CommandIndex];
        }
        #endregion
    }
}
