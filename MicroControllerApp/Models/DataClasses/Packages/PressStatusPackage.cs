﻿using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public enum PressStatusTypes
    {
        Available = 1 << 0,
        SampleOn = 1 << 1,
        SampleOff = 1 << 2
    }

    public class PressStatusPackage : DataPackage
    {
        #region Const
        public const int IdIndex = 2;
        public const int PcCommandIndex = 3;
        public const int StatusIndex = 7;
        #endregion

        #region Getter
        public byte ID { get; private set; }
        public uint PcCommand { get; private set; }
        public uint Status { get; private set; }
        #endregion

        #region Constructor
        public PressStatusPackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            this.ID = buffer[IdIndex];
            this.PcCommand = BitConverter.ToUInt32(buffer, PcCommandIndex);
            this.Status = BitConverter.ToUInt32(buffer, StatusIndex);
        }
        #endregion
    }
}
