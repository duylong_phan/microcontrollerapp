﻿using MicroControllerApp.Models.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class AccValue
    {
        public short X { get; private set; }
        public short Y { get; private set; }
        public short Z { get; private set; }

        public AccValue(short x, short y, short z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }
    }

    public class AccSamplePackage : DataPackage
    {
        #region Const
        public const int AccAmount = 4;
        public const int AxisAmount = 3;
        public const int SampleIndex = 2;
        #endregion


        public AccValue[] Samples { get; private set; }

        public AccSamplePackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            this.Samples = new AccValue[AccAmount];

            int index = SampleIndex;
            for (int i = 0; i < AccAmount; i++)
            {
                var x = BitConverter.ToInt16(buffer, index);
                index += 2;
                var y = BitConverter.ToInt16(buffer, index);
                index += 2;
                var z = BitConverter.ToInt16(buffer, index);
                index += 2;

                this.Samples[i] = new AccValue(x, y, z);
            }
        }

        #region Public Method
        public override byte[] GetBytes()
        {
            byte[] data = null;

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.Type);
                writer.Write((byte)0);
                for (int i = 0; i < AccAmount; i++)
                {
                    writer.Write(this.Samples[i].X);
                    writer.Write(this.Samples[i].Y);
                    writer.Write(this.Samples[i].Z);
                }

                data = stream.ToArray();
                data[1] = (byte)data.Length;
            }

            return data;
        }
        #endregion
    }
}
