﻿using MicroControllerApp.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class MotorCtrSamplePackage : DataPackage
    {
        #region Const
        public const int SampleFlagIndex = 2;
        public const int BatteryIndex = 6;
        public const int PulseIndex = 8;
        public const int Pwm_FrequencyIndex = 10;

        public const double BatteryFactor = 0.0032;
        public const double PulseFactor = 0.051;
        public const double FrequencyFactor = 0.001;
        #endregion

        #region Getter
        public uint SampleFlag { get; private set; }
        public double Battery { get; private set; }
        public double Pulse { get; private set; }
        public double Pwm_Frequency { get; private set; }
        #endregion

        #region Constructor
        public MotorCtrSamplePackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            this.SampleFlag = BitConverter.ToUInt32(buffer, SampleFlagIndex);
            this.Battery = BitConverter.ToUInt16(buffer, BatteryIndex) * BatteryFactor;
            this.Pulse = BitConverter.ToUInt16(buffer, PulseIndex) * PulseFactor;
            this.Pwm_Frequency = BitConverter.ToUInt32(buffer, Pwm_FrequencyIndex) * FrequencyFactor;
        }
        #endregion
    }
}
