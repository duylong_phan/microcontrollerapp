﻿using MicroControllerApp.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Infos
{
    public class PressIdGroup 
    {
        #region Getter
        public int ID { get; private set; }
        #endregion

        #region Getter, setter
        public int Amount { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        #endregion

        #region Constructor
        public PressIdGroup(int id)
        {
            this.ID = id;
            this.Amount = 0;
            this.StartTime = DateTime.Now;
            this.EndTime = new DateTime(1, 1, 1, 1, 1, 1);
        }
        #endregion
    }
}
