﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.Views.Options;
using MicroControllerApp.Models.DataClasses.Settings;

namespace MicroControllerApp.Models.ViewClasses.Options
{
    public class MicroTaskOption : OptionContentBase<MicroTasks>
    {
        public MicroTaskOption(string text, MicroTasks type)
            : base(text, type)
        {

        }
    }
}
