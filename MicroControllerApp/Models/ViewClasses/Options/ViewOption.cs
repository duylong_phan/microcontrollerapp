﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.Views.Options;
using MicroControllerApp.Models.DataClasses.Settings;

namespace MicroControllerApp.Models.ViewClasses.Options
{
    public class ViewOption : OptionContentBase<AppViews>
    {
        public ViewOption(string text, AppViews type)
            : base(text, type)
        {

        }
    }
}
