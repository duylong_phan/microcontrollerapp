﻿using LongModel.Views.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Options
{
    public class RfTransIntervalOption : OptionBase<byte>
    {
        public RfTransIntervalOption(string text, byte type)
            : base(text, type)
        {

        }
    }
}
