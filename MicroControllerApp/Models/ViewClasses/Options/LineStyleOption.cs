﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using LongModel.Views.Options;

namespace MicroControllerApp.Models.ViewClasses.Options
{
    public class LineStyleOption : OptionBase<LineStyle>
    {
        public LineStyleOption(string text, LineStyle type)
            : base(text, type)
        {

        }
    }
}
