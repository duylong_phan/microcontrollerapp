﻿using LongModel.Views.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Options
{
    public class MotorCtrBackwardOption : OptionBase<byte>
    {
        public MotorCtrBackwardOption(string text, byte type)
            : base(text, type)
        {

        }
    }
}
