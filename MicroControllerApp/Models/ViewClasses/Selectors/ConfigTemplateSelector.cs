﻿using LongModel.Models.IO.Files;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using MicroControllerApp.Models.ViewClasses.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MicroControllerApp.Models.ViewClasses.Selectors
{
    public class ConfigTemplateSelector : DataTemplateSelector
    {
        #region Getter, setter
        public DataTemplate Serial { get; set; }
        public DataTemplate Usb { get; set; }        
        public DataTemplate Graphic { get; set; }
        public DataTemplate Local { get; set; }
        public DataTemplate Server { get; set; }
        public DataTemplate Remote { get; set; }
        public DataTemplate File { get; set; }
        public DataTemplate PressFile { get; set; }
        public DataTemplate FileChanged { get; set; }

        public DataTemplate CapacitorSetting { get; set; }
        public DataTemplate AccelerometerSetting { get; set; }
        public DataTemplate PressSetting { get; set; }
        public DataTemplate uBalanceSetting { get; set; }
        public DataTemplate RfTransSetting { get; set; }
        public DataTemplate MotorCtrSetting { get; set; }
        public DataTemplate MotorBalanceSetting { get; set; }
        #endregion

        #region Public Method
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DataTemplate template = null;

            if (item is PressSetting)               template = this.PressSetting;            
            else if(item is CapSetting)             template = this.CapacitorSetting;
            else if(item is uBalanceSetting)        template = this.uBalanceSetting;
            else if(item is RfTransSetting)         template = this.RfTransSetting;   
            else if(item is MotorCtrSetting)        template = this.MotorCtrSetting;
            else if(item is AccSetting)             template = this.AccelerometerSetting;
            //Sub
            else if (item is SerialPortSetting)     template = this.Serial;
            else if (item is UsbSetting)            template = this.Usb;            
            else if(item is GraphicSetting)         template = this.Graphic;
            else if (item is LocalSetting)          template = this.Local;
            else if (item is ServerSetting)         template = this.Server;
            else if(item is RemoteSetting)          template = this.Remote;
            else if(item is FileSingleSetting)      template = this.File;
            else if(item is PressFileSetting)       template = this.PressFile;
            else if(item is FileChangedSetting)     template = this.FileChanged;
            else if(item is MotorUnbalanceSetting)    template = this.MotorBalanceSetting;
            return template;
        }
        #endregion
    }
}
