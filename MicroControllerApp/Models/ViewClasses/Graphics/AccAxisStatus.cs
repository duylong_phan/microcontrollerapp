﻿using LongModel.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Graphics
{
    public class AccAxisVisibilityChangedEventArgs : EventArgs
    {
        public int Index { get; private set; }
        public object Axis { get; private set; }
        public bool CanShow { get; private set; }

        public AccAxisVisibilityChangedEventArgs(int index, object axis, bool canShow)
        {
            this.Index = index;
            this.Axis = axis;
            this.CanShow = canShow;
        }
    }

    public class AccAxisStatus : ObservableObject
    {
        public event EventHandler<AccAxisVisibilityChangedEventArgs> VisibilityChanged;
        protected void OnVisibilityChanged(AccAxisVisibilityChangedEventArgs arg)
        {
            var handler = VisibilityChanged;
            if (handler != null && _canNotify)
            {
                handler(this, arg);
            }
        }

        private object _x;
        private object _y;
        private object _z;
        private bool _showX;
        private bool _showY;
        private bool _showZ;
        private bool _canNotify;

        public bool ShowX
        {
            get { return _showX; }
            set
            {
                var isChanged = this.SetProperty(ref _showX, value);
                if (isChanged)
                {
                    OnVisibilityChanged(new AccAxisVisibilityChangedEventArgs(this.Index, _x, _showX));
                }
            }
        }        
        public bool ShowY
        {
            get { return _showY; }
            set
            {
                var isChanged = this.SetProperty(ref _showY, value);
                if (isChanged)
                {
                    OnVisibilityChanged(new AccAxisVisibilityChangedEventArgs(this.Index, _y, _showY));
                }
            }
        }        
        public bool ShowZ
        {
            get { return _showZ; }
            set
            {
                var isChanged = this.SetProperty(ref _showZ, value);
                if (isChanged)
                {
                    OnVisibilityChanged(new AccAxisVisibilityChangedEventArgs(this.Index, _z, _showZ));
                }
            }
        }

        public int Index { get; private set; }
        
        public AccAxisStatus(int index)
        {
            this.Index = index;            
            _showX = _showY = _showZ = true;
            _canNotify = true;
        }

        public void SetAxis(object x, object y, object z)
        {
            _canNotify = false;
            //set
            _x = x;
            _y = y;
            _z = z;
            //reset
            this.ShowX = true;
            this.ShowY = true;
            this.ShowZ = true;
            _canNotify = true;
        }
    }
}
