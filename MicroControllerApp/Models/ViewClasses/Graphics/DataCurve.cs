﻿using LongModel.Helpers;
using MicroControllerApp.Interfaces;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Graphics
{
    public class DataCurve : LineSeries, ICurveInfo
    {
        public DataCurve()
        {

        }

        public void CopyProperties(ICurveInfo item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }
    }
}
