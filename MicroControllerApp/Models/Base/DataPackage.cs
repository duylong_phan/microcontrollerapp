﻿using MicroControllerApp.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LongModel.BaseClasses;
using LongInterface.Models;

namespace MicroControllerApp.Models.Base
{
    public abstract class DataPackage : ObservableObject, IBuffer
    {
        #region Const
        public const int HeaderIndex = 0;
        public const int LengthIndex = 1;
        #endregion

        #region Static
        public static byte[] Header { get; private set; }

        static DataPackage()
        {
            Header = new byte[4] { (byte)'#', (byte)'#', (byte)'#', (byte)'#' };
        }
        #endregion

        #region Getter        
        public byte Type { get; private set; }
        public byte[] Buffer { get; protected set; }
        #endregion

        #region Getter, Setter
        public object View { get; set; }
        #endregion

        #region Constructor
        public DataPackage(byte type, byte[] buffer)
        {
            this.Type = type;
            this.Buffer = buffer;
        }
        #endregion

        #region Abstract
        public virtual byte[] GetBytes()
        {
            return this.Buffer;
        }
        #endregion
    }
}
