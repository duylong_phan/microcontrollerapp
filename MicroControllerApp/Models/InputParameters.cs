﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using LongModel.Models;
using LongInterface.Models.IO.Files;
using MicroControllerApp.Models.ViewClasses.Options;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using System;

namespace MicroControllerApp.Models
{
    public class InputParameters : Model, IMoveBothMotor
    {
        #region Getter, setter
        private Configuration currentConfig;
        private bool motorCtr_StoreAction;
        private bool motorCtr_AutoStartAfter;
        private bool motorCtr_CanAutoBalance;
        private ushort motorCtr_StartDelay;        
        private ushort motorCtr_PwmPeriod;
        private ushort motorCtr_MoveBothDelay;
        private OutPinHandlerSetting selectedOutPin;
        
        public Configuration CurrentConfig
        {
            get { return this.currentConfig; }
            set
            {
                this.SetProperty(ref this.currentConfig, value);
            }
        }
        public bool MotorCtr_StoreAction
        {
            get { return this.motorCtr_StoreAction; }
            set
            {
                this.SetProperty(ref this.motorCtr_StoreAction, value);
            }
        }
        public bool MotorCtr_AutoStartAfter
        {
            get { return this.motorCtr_AutoStartAfter; }
            set
            {
                this.SetProperty(ref this.motorCtr_AutoStartAfter, value);
            }
        }
        public bool MotorCtr_CanAutoBalance
        {
            get { return motorCtr_CanAutoBalance; }
            set
            {
                lock (this.syncObject)
                {
                    this.motorCtr_CanAutoBalance = value;
                }
            }
        }
        public ushort MotorCtr_StartDelay
        {
            get { return this.motorCtr_StartDelay; }
            set
            {
                this.SetProperty(ref this.motorCtr_StartDelay, value);
            }
        }                
        public ushort MotorCtr_PwmPeriod
        {
            get { return this.motorCtr_PwmPeriod; }
            set
            {
                this.SetProperty(ref this.motorCtr_PwmPeriod, value);
            }
        }
        public ushort MotorCtr_MoveBothDelay
        {
            get { return this.motorCtr_MoveBothDelay; }
            set
            {
                this.SetProperty(ref this.motorCtr_MoveBothDelay, value);
            }
        }
        public OutPinHandlerSetting SelectedOutPin
        {
            get { return this.selectedOutPin; }
            set
            {
                this.SetProperty(ref this.selectedOutPin, value);
            }
        }
        #endregion

        #region Getter
        //Task
        public List<MicroTaskOption> TaskOptions { get; private set; }
        public List<AppPositionOption> PositionOptions { get; private set; }
        public LocalSetting LocalSetting { get; private set; }
        public List<FileChangedResponseOption> FileChangedResponseCollection { get; private set; }
        public List<OutPinHandlerSetting> OutPinCollection { get; private set; }
        public List<OutPinCommandOption> OutPinCommandCollection { get; private set; }

        //serial
        public ObservableCollection<string> PortCollection { get; private set; }
        public List<int> BaudRateCollection { get; private set; }
        public List<int> DataBitCollection { get; private set; }
        public List<Parity> ParityCollection { get; private set; }
        public List<StopBits> StopBitsCollection { get; private set; }

        //Graphic
        public List<LineStyleOption> LineStyleCollection { get; private set; }

        //uBalance        
        public List<uBalanceBitOption> uBalance_ActionCollection { get; private set; }

        //RfTrans
        public List<RfTransIntervalOption> RfTrans_IntervalCollection { get; private set; }

        //MotorCtr
        public IMoveMotor MotorCtr_Motor1Move { get; private set; }
        public IMoveMotor MotorCtr_Motor2Move { get; private set; }
        public List<MotorCtrActionOption> MotorCtr_ActionCollection { get; private set; }
        public List<MotorCtrBackwardOption> MotorCtr_BackwardCollection { get; private set; }

        #endregion

        #region Field
        private object syncObject;
        #endregion

        #region Constructor
        public InputParameters()
        {
            this.syncObject = new object();

            #region Task
            this.TaskOptions = new List<MicroTaskOption>();
            this.TaskOptions.Add(new MicroTaskOption("No task", MicroTasks.Non));
            this.TaskOptions.Add(new MicroTaskOption("Detect Hose Clamp Pliers closed", MicroTasks.Press));
            this.TaskOptions.Add(new MicroTaskOption("Measure Capacitors", MicroTasks.Capacitor));
            this.TaskOptions.Add(new MicroTaskOption("Measure Acceleration", MicroTasks.Acceleration));
            this.TaskOptions.Add(new MicroTaskOption("uBalance System", MicroTasks.uBalance));
            this.TaskOptions.Add(new MicroTaskOption("Test RF Transceiver", MicroTasks.RfTrans));
            this.TaskOptions.Add(new MicroTaskOption("Motor Controller System", MicroTasks.MotorCtr));

            this.PositionOptions = new List<AppPositionOption>();
            this.PositionOptions.Add(new AppPositionOption("Local", AppPositions.Local));
            this.PositionOptions.Add(new AppPositionOption("Remote", AppPositions.Remote));
            
            this.LocalSetting = new LocalSetting();
            this.LocalSetting.Add(new PhysicalPortOption("Serial", PhysicalPorts.Serial));
            this.LocalSetting.Add(new PhysicalPortOption("USB", PhysicalPorts.USB));
            this.LocalSetting.Add(new PhysicalPortOption("Web Server", PhysicalPorts.WebServer));

            //for Position => Local
            this.PositionOptions[0].Content = this.LocalSetting;

            this.FileChangedResponseCollection = new List<FileChangedResponseOption>();
            this.FileChangedResponseCollection.Add(new FileChangedResponseOption("New line", FileChangedResponses.NewLine));
            this.FileChangedResponseCollection.Add(new FileChangedResponseOption("XML Element", FileChangedResponses.XmlElement));
            this.FileChangedResponseCollection.Add(new FileChangedResponseOption("Fix Length", FileChangedResponses.FixLength));
            this.FileChangedResponseCollection.Add(new FileChangedResponseOption("Customed Action", FileChangedResponses.CustomedAction));

            this.OutPinCollection = new List<OutPinHandlerSetting>();
            this.OutPinCollection.Add(new OutPinHandlerSetting("Output Pin 0, P1.4", 0));
            this.OutPinCollection.Add(new OutPinHandlerSetting("Output Pin 1, P1.5", 1));
            this.OutPinCollection.Add(new OutPinHandlerSetting("Output Pin 2, P1.6", 2));
            this.OutPinCollection.Add(new OutPinHandlerSetting("Output Pin 3, P1.7", 3));
            this.selectedOutPin = this.OutPinCollection[0];

            this.OutPinCommandCollection = new List<OutPinCommandOption>();
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Do nothing", OutPinCommands.Non));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Signal = 0", OutPinCommands.Reset));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Signal = 1", OutPinCommands.Set));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Toggle Signal", OutPinCommands.Toggle));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Positive Pulse", OutPinCommands.PosTrigger));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Negative Pulse", OutPinCommands.NegTrigger));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Rectagle Signal", OutPinCommands.Rect));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Manual PWM", OutPinCommands.PWM));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Auto PWM", OutPinCommands.PWM_Auto));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Manual PWM for standard Servo", OutPinCommands.PWM_Servo));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Auto PWM for standard Servo", OutPinCommands.PWM_Servo_Auto));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Manual PWM for SV1270TG Servo", OutPinCommands.PWM_Servo_SV1270TG));
            this.OutPinCommandCollection.Add(new OutPinCommandOption("Auto PWM for SV1270TG Servo", OutPinCommands.PWM_Servo_SV1270TG_Auto));
            #endregion

            #region Serial
            this.PortCollection = new ObservableCollection<string>();
            this.BaudRateCollection = new List<int>() { 9600, 56000, 115200 };
            this.DataBitCollection = new List<int>() { 5, 6, 7, 8 };
            this.ParityCollection = new List<Parity>() { Parity.None, Parity.Odd, Parity.Even, Parity.Mark, Parity.Space };
            this.StopBitsCollection = new List<StopBits>() { StopBits.None, StopBits.One, StopBits.Two, StopBits.OnePointFive };
            #endregion

            #region Graphic
            this.LineStyleCollection = new List<LineStyleOption>();
            this.LineStyleCollection.Add(new LineStyleOption("Solid Line", OxyPlot.LineStyle.Solid));
            this.LineStyleCollection.Add(new LineStyleOption("Dash Line", OxyPlot.LineStyle.Dash));
            this.LineStyleCollection.Add(new LineStyleOption("Dot Line", OxyPlot.LineStyle.Dot));
            this.LineStyleCollection.Add(new LineStyleOption("Dash Dot Line", OxyPlot.LineStyle.DashDot));
            this.LineStyleCollection.Add(new LineStyleOption("Long Dash Line", OxyPlot.LineStyle.LongDash));
            #endregion

            #region uBalance
            this.uBalance_ActionCollection = new List<uBalanceBitOption>();
            this.uBalance_ActionCollection.Add(new uBalanceBitOption("Trigger", uBalanceChannelInfoBits.Trigger));
            this.uBalance_ActionCollection.Add(new uBalanceBitOption("Delay", uBalanceChannelInfoBits.Delay));
            #endregion

            #region RfTrans
            this.RfTrans_IntervalCollection = new List<RfTransIntervalOption>();
            this.RfTrans_IntervalCollection.Add(new RfTransIntervalOption("As soon as posible", 0));
            for (int i = 1; i <= 10; i++)
            {
                this.RfTrans_IntervalCollection.Add(new RfTransIntervalOption(string.Format("{0} ms", i * 5), (byte)(i * 5)));
            }
            #endregion

            #region MotorCtr
            this.motorCtr_StoreAction = false;
            this.motorCtr_AutoStartAfter = false;
            this.motorCtr_CanAutoBalance = false;
            this.motorCtr_StartDelay = 1000;           
            this.motorCtr_PwmPeriod = MotorCtrMotorAction.BestPeriod;
            this.motorCtr_MoveBothDelay = 500;

            this.MotorCtr_Motor1Move = new MotorMoveSetting((byte)MotorControl1Flags.Motor1, true, 1000);
            this.MotorCtr_Motor2Move = new MotorMoveSetting((byte)MotorControl1Flags.Motor2, true, 1000);
            
            this.MotorCtr_ActionCollection = new List<MotorCtrActionOption>();
            this.MotorCtr_ActionCollection.Add(new MotorCtrActionOption("Trigger", (byte)MotorControl1Flags.Trigger));
            this.MotorCtr_ActionCollection.Add(new MotorCtrActionOption("Delay", (byte)MotorControl1Flags.Delay));

            this.MotorCtr_BackwardCollection = new List<MotorCtrBackwardOption>();
            this.MotorCtr_BackwardCollection.Add(new MotorCtrBackwardOption("After Pulse", byte.MinValue));
            this.MotorCtr_BackwardCollection.Add(new MotorCtrBackwardOption("After Package", (byte)MotorControl2Flags.AfterPackage));
            #endregion
        }
        #endregion

        #region Public Method
        public void UpdateLeftView(AppViews view)
        {
            if (this.CurrentConfig != null && this.CurrentConfig.CurrentSetting != null)
            {
                this.CurrentConfig.CurrentSetting.LeftView = view;
            }
        }

        public void UpdateRightView(AppViews view)
        {
            if (this.CurrentConfig != null && this.CurrentConfig.CurrentSetting != null)
            {
                this.CurrentConfig.CurrentSetting.RightView = view;
            }
        }
        #endregion
    }
}
