﻿using MicroControllerApp.Models.ViewClasses.Infos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MicroControllerApp.Views.Dialogs
{   
    public partial class FileNameInfoWindow : Window
    {
        public FileNameInfoWindow(Window owner, List<FileNameInfo> collection)
        {
            InitializeComponent();
            this.Owner = owner;
            this.DataContext = collection;
        }
    }
}
