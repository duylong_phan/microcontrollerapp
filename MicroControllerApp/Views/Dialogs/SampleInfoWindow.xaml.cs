﻿using MicroControllerApp.Models.DataClasses.Settings.Sub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MicroControllerApp.Views.Dialogs
{   
    public partial class SampleInfoWindow : Window
    {
        public SampleInfoWindow(Window owner, SampleInfo info)
        {
            InitializeComponent();
            this.Owner = owner;
            this.DataContext = info;
            if (info.Frequecy > 0)
            {
                this.freqTxt.Text = info.Frequecy.ToString(App.NumberFormat);
            }
            else
            {
                this.freqTxt.Text = "Unknown";
            }
        }
    }
}
