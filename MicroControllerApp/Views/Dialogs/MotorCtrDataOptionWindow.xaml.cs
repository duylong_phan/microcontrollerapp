﻿using LongWpfUI.Helpers;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MicroControllerApp.Views.Dialogs
{
    public partial class MotorCtrDataOptionWindow : Window
    {
        #region Constructor
        public MotorCtrDataOptionWindow(Window owner, MotorCtrDataOption option)
        {
            InitializeComponent();
            this.Owner = owner;
            this.DataContext = option;
            this.Closing += MotorCtrDataOptionWindow_Closing;
        }
        #endregion

        #region Event Handler
        private void MotorCtrDataOptionWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                var collection = ControlHelper.GetChildrenControl<TextBox>(this.mainPanel);
                foreach (var item in collection)
                {
                    if (item != null && Validation.GetHasError(item))
                    {
                        throw new Exception("Invalid input value: " + item.Text);
                    }
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Invalid Parameters", MessageBoxButton.OK, MessageBoxImage.Information);
                e.Cancel = true;
            }
        }
        #endregion
    }
}
