﻿using MicroControllerApp.Models.ViewClasses.Infos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LongWpfUI.Windows;
using LongWpfUI.Models.Commands;

namespace MicroControllerApp.Views.Dialogs
{
    public partial class PressFilterWindow : DialogBase
    {
        #region Field
        private PressFilterInfo info;
        #endregion

        #region Constructor
        public PressFilterWindow(Window owner, PressFilterInfo info)
        {            
            this.Owner = owner;
            this.info = info;
            this.DataContext = info;

            InitializeComponent();
            this.idPanel.IsEnabled = info.UseId;
            this.timePanel.IsEnabled = info.UseTime;
            this.SaveCommand = new WpfRelayCommand(Exe_Save);
            this.CancelCommand = new WpfRelayCommand(Exe_Cancel);
        }
        #endregion

        #region Private Method
        protected override void Exe_Save()
        {
            string message = string.Empty;
            if (HasInputError(this.MainPanel, out message))
            {
                MessageBox.Show(message, "Invalid Input value", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            this.DialogResult = true;
            this.Close();
        }

        protected override void Exe_Cancel()
        {
            this.DialogResult = false;
            this.Close();
        }
        #endregion

        #region Event Handler
        private void UseID_CheckChanged(object sender, RoutedEventArgs e)
        {
            if (this.idPanel != null && this.info != null)
            {
                this.idPanel.IsEnabled = this.info.UseId;
            }
        }

        private void UseTime_CheckChanged(object sender, RoutedEventArgs e)
        {
            if (this.timePanel != null && this.info != null)
            {
                this.timePanel.IsEnabled = this.info.UseTime;
            }
        }
        
        #endregion
    }
}
