﻿using LongWpfUI.Helpers;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MicroControllerApp.Views.Dialogs
{    
    public partial class DeviceSettingEditorWindow : Window
    {
        #region Field
        private DataPackage package;
        private Panel modelPanel;
        private MicroTasks task;
        #endregion

        #region Constructor
        public DeviceSettingEditorWindow(Window owner, DataPackage package, MicroTasks task)
        {
            this.package = package;
            this.package.View = this;
            this.Owner = owner;
            this.task = task;
            InitializeComponent();

            DataTemplate template = null;
            string title = "Slave device setting";
            switch (task)
            {
                case MicroTasks.Press:
                    template = this.Resources["pressSlaveSettingTemplate"] as DataTemplate;                    
                    break;

                case MicroTasks.Capacitor:
                    break;

                case MicroTasks.Acceleration:
                    break;

                case MicroTasks.uBalance:
                    break;

                case MicroTasks.RfTrans:
                    break;

                case MicroTasks.Non:
                default:
                    template = this.Resources["masterSettingTemplate"] as DataTemplate;
                    title = "Master device setting";
                    break;
            }
            this.contentControl.ContentTemplate = template;
            this.Title = title;
        }
        #endregion

        #region Public Method
        public bool HasError(out string message)
        {
            bool hasError = false;
            message = string.Empty;
            if (this.modelPanel == null)
            {
                throw new Exception("Main panel is missing");
            }

            var collection = ControlHelper.GetChildrenControl<TextBox>(this.modelPanel);
            foreach (var item in collection)
            {
                if (item != null && Validation.GetHasError(item))
                {
                    hasError = true;
                    message = "Invalid input value: " + item.Text;
                    break;
                }
            }

            return hasError;
        }
        #endregion

        #region Event Handler
        private void WriteSetting_Loaded(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null && App.ViewModel != null)
            {
                switch (this.task)
                {
                    case MicroTasks.Press:
                        button.Command = App.ViewModel.Press_WriteSetting;
                        break;

                    case MicroTasks.Capacitor:
                        break;

                    case MicroTasks.Acceleration:
                        break;

                    case MicroTasks.uBalance:
                        break;

                    case MicroTasks.RfTrans:
                        break;

                    case MicroTasks.Non:
                    default:
                        button.Command = App.ViewModel.WriteMasterSetting;
                        break;
                }
                button.CommandParameter = this.package;
            }
        }

        private void ModelPanel_Initialized(object sender, EventArgs e)
        {
            this.modelPanel = sender as Panel;
            if (this.modelPanel != null)
            {
                this.modelPanel.DataContext = this.package;
            }
        }
        #endregion
    }
}
