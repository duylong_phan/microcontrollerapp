﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MicroControllerApp.Views
{
    public enum SexTypes
    {
        Male,
        Female
    }

    public class Person
    {
        public string Name { get; private set; }
        public SexTypes Type { get; private set; }
        public Person(string name, SexTypes type)
        {
            this.Name = name;
            this.Type = type;
        }
    }

    public class TestCollection : List<Person>
    {

        public TestCollection()
        {
            this.Add(new Person("Persion 1", SexTypes.Female));
            this.Add(new Person("Persion 2", SexTypes.Female));
            this.Add(new Person("Persion 3", SexTypes.Female));
            this.Add(new Person("Persion 4", SexTypes.Female));
            this.Add(new Person("Persion 5", SexTypes.Male));
            this.Add(new Person("Persion 6", SexTypes.Male));
            this.Add(new Person("Persion 7", SexTypes.Male));
        }

    }
       
    public partial class DummyView : Window
    {
        public DummyView()
        {
            InitializeComponent();
        }
    }
}
