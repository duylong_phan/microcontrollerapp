# Before the device is connected to computer's USB port, please make sure #
* the computer has the latest Windows's update
* the internet connection is available

## Connect PSOC4 to PC ##

![PSOC4_Driver_1.png](https://bitbucket.org/repo/5z4x5A/images/2991205211-PSOC4_Driver_1.png)

## Automatic installation PSOC4's driver ##

![PSOC4_Driver_2.png](https://bitbucket.org/repo/5z4x5A/images/3709660279-PSOC4_Driver_2.png)

## Open Device manager to check if driver is correctly installed ##

![DeviceManager.png](https://bitbucket.org/repo/5z4x5A/images/684765062-DeviceManager.png)

## Find Communication Port ##

![PSOC4_Driver_3.png](https://bitbucket.org/repo/5z4x5A/images/2012275015-PSOC4_Driver_3.png)

## Important ##
* The drive is only required to install, when the device is connected to Computer on the first time.
* The COM# must be matched in the Connection's Port in Configuration Dialog
