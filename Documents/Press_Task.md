# Basic Functions # 

![Software_Basic_1.png](https://bitbucket.org/repo/5z4x5A/images/1632030300-Software_Basic_1.png)

1. Create a new configuration
1. Load configuration from a file
1. Edit current configuration
1. Save current configuration into file
1. View on the left side (only available after software is configured)
1. View on the right side
1. Open Data Folder of the software
1. Show Help information

# Create a new configuration #

![Software_Basic_2.png](https://bitbucket.org/repo/5z4x5A/images/2450028503-Software_Basic_2.png)

1. Select Task
1. Select Computer Position, Local (Computer connects directly to device), Remote (Computer on same IP Network)
1. Select Communicaiton's Port, which must be matched COM#, in device manager
1. Refresh list of Port
1. When local computer supports server's operation

# Server's operation Setting #

![Software_Press_1.png](https://bitbucket.org/repo/5z4x5A/images/2329774615-Software_Press_1.png)

1. Communication's Port
1. Show current IP address of computer
1. Register Port in Fireware, which must be done on the first time
1. Register Prefix in Computer Domain, which must be done on the first time

# Remote's operation Setting #

![Software_Press_2.png](https://bitbucket.org/repo/5z4x5A/images/656524912-Software_Press_2.png)

1. Select Postion as Remote
1. Communication's Port, which must be matched Server's Setting
1. IP address of the server
1. Amount of time to verify a timeout communication
1. Period of time to exchange information with server
1. Check if computer can communicate with server with the above setting

# Graphic for Sensor's output value #

![Software_Basic_5.png](https://bitbucket.org/repo/5z4x5A/images/948628379-Software_Basic_5.png)

1. Select Graphic's setting
1. Max amount of  points, which will be shown on graphic
1. Show Label of the curve on top left side on graphic
1. Edit Axis's parameter, such as name, color
1. Edit Cuve's parameter, such as name, color


# History file for clamping detection #

![Software_Press_3.png](https://bitbucket.org/repo/5z4x5A/images/4082351263-Software_Press_3.png)

1. Select History File's setting
1. Enable Software to initialize resource for History File function
1. Enable Function after the **Start** function is executed
1. Select Directory, where the history file will be saved

# Matlab File's Setting #

![Software_Basic_6.png](https://bitbucket.org/repo/5z4x5A/images/2775993239-Software_Basic_6.png)

1. Select Matlab File's Setting
1. Enable Software to initialize resource for Matlab File function
1. Enable Function after the **Start** function is executed
1. Select Directory, where the file will be saved
1. File's prefix name
1. Amount of sample in one file
1. Use Index as file's suffix
1. Collect data only in given time

# After the configuration is done, save and select view #

![Software_Basic_3.png](https://bitbucket.org/repo/5z4x5A/images/1959606356-Software_Basic_3.png)

1. View on the Left side
1. View on the Right side

# Basic Operation #

![Software_Basic_4.png](https://bitbucket.org/repo/5z4x5A/images/2652634170-Software_Basic_4.png)

1. Select Oepration Tab
1. Start Operation, which allows the RF Communication, and other functions are enabled
1. Stop Operation before the software is closed
1. Read Master Device's setting, only available for Local Computer

# Slave device's functions #

![Software_Press_4.png](https://bitbucket.org/repo/5z4x5A/images/3409377014-Software_Press_4.png)

1. Start collecting sensor's output value
1. Stop collecting sernsor's output value
1. View the Matlab file's column structure
1. Check if slave device is available
1. Enable/Disable Debug Function in slave device
1. Read Slave device Setting

# Edit and update slave device's setting #

![Software_Press_8.png](https://bitbucket.org/repo/5z4x5A/images/153891432-Software_Press_8.png)

1. Update Setting Buffer

# Switch to Clamping detection's table view #

![Software_Press_5.png](https://bitbucket.org/repo/5z4x5A/images/1536197485-Software_Press_5.png)

1. Device ID
1. Amount of clamping action is detected
1. Time, when the software received information from slave device

# Remote Operation view #

![Software_Press_6.png](https://bitbucket.org/repo/5z4x5A/images/1131078354-Software_Press_6.png)

1. Request Information of all clamping detections' History file on server
1. File Name
1. File Size
1. Download and load file content

# History File content #

![Software_Press_7.png](https://bitbucket.org/repo/5z4x5A/images/2413048805-Software_Press_7.png)

1. Overview of clamping detection information
1. Clamping detection information after filtering


# Support #
For more information, and support please contact [phanduylong@gmail.com](Link URL)