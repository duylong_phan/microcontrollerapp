This Software is written in order to handle the communication between the computer and microcontroller.

In order to use the firmware, there are two method

# The project can be cloned in local computer with SourceTree Software. #

1. Select **Download** Option
1. Select **Download repository** Option
![Bitbuck_Clone.PNG](https://bitbucket.org/repo/BnkEG9/images/3720376948-Bitbuck_Clone.PNG)

# Or download the whole project as Zip File #

1. Select **Clone** Option
1. Select **Clone in SourceTree**, the SourceTree Software should already be installed in the Computer
![Bitbuck_Download.PNG](https://bitbucket.org/repo/BnkEG9/images/3931022052-Bitbuck_Download.PNG)

# For more detail, please read: #
## Driver ##

[Install PSOC4's Driver](https://bitbucket.org/duylong_phan/microcontrollerapp/src/dbf9ad529a95459a0d91e119e5cbf6b9a6773d25/Documents/PSOC4_Driver.md)

[Install PSOC5's Driver](https://bitbucket.org/duylong_phan/microcontrollerapp/src/dbf9ad529a95459a0d91e119e5cbf6b9a6773d25/Documents/PSOC5_Driver.md)


## Configuration and Usage ##

[Hose Clamp Plier](https://bitbucket.org/duylong_phan/microcontrollerapp/src/dbf9ad529a95459a0d91e119e5cbf6b9a6773d25/Documents/Press_Task.md)